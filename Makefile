.PHONY: backend frontend

backend:
	cd backend
	docker build --tag flask-app .
	docker run -p 5000:5000 flask-app

frontend:
	cd frontend
	docker build --tag react-app .
	docker run -p 3000:3000 react-app

jest-tests:
	cd frontend
	npm install
	npm test -- --watchAll=false --silent

selenium-tests:
	python3 frontend/gui_tests/seleniumTests.py
