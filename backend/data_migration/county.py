from models import County
import json
from models import db
from pprint import pprint

with open("data_migration/county_data.json") as f:
    data = json.load(f)


def add_data():
    for key, value in data.items():
        if value.get("Median_Household_Income_2019"):
            median_household_income = int(value.get("Median_Household_Income_2019"))
        else:
            median_household_income = None

        if value.get("avg_edu_bach_attain"):
            avg_edu_bach_attain = float(value.get("avg_edu_bach_attain"))
        else:
            avg_edu_bach_attain = None

        if value.get("avg_edu_hs_attain"):
            avg_edu_hs_attain = float(value.get("avg_edu_hs_attain"))
        else:
            avg_edu_hs_attain = None

        if value.get("POVALL_2019"):
            poverty = value.get("POVALL_2019")
        else:
            poverty = None

        if not value.get("FIPS Code"):
            continue
        fips = int(value.get("FIPS Code"))

        county = County(
            id=fips,
            name=value.get("Area name") or "UNKNOWN",
            median_household_income=median_household_income,
            unemployment=value.get("Unemployment_rate_2019"),
            state=value.get("State"),
            population=value.get("Population 2020"),
            avg_edu_bach_attain=avg_edu_bach_attain,
            avg_edu_hs_attain=avg_edu_hs_attain,
            poverty=poverty,
        )
        db.session.add(county)

    db.session.commit()
