from models import Brewery, County
import json
from models import db
from pprint import pprint
import csv

with open("data_migration/breweries_data.json") as f:
    data = json.load(f)

zip_to_county = {}
# use csv to map postal code to county
with open("data_migration/zip_code_database.csv") as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        zip_to_county[row["zip"]] = row["county"]


def add_data():
    test_county = County(
        id=1,
        name="test_county",
        state="TX"
    )
    db.session.add(test_county)
    db.session.commit()
    lookup_table = {
        (county.name.lower(), county.state.lower()): county
        for county in County.query.all()
    }
    uid = 0
    for i, item in enumerate(data):
        county_name = zip_to_county[item.get(
            "postal_code").split("-")[0]]
        if not county_name:
            continue
        state_abbrev = "TX"  # all TX
        lookup_query = (county_name.lower(), state_abbrev.lower())
        matched_county = lookup_table.get(lookup_query)
        if not matched_county:
            # matched_county = test_county
            continue
        latitude = float(item.get("latitude") or 0)
        longitude = float(item.get("longitude") or 0)

        if not latitude or latitude == 0 or not longitude or longitude == 0:
            # print("skip", i)
            continue

        brewery = Brewery(
            id=uid,
            name=item.get("name"),
            brewery_type=item.get("brewery_type"),
            latitude=latitude,
            longitude=longitude,
            address=item.get("street"),
            county_id=matched_county.id,
            city=item.get("city"),
            state=item.get("state"),
            phone=item.get("phone"),
            website=item.get("website_url"),  # duplicate?
            url=item.get("website_url"),
            county=matched_county
        )
        uid += 1
        db.session.add(brewery)

    db.session.commit()
