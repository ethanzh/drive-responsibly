import json
from models import db, TrafficIncident, County
from datetime import datetime

with open("data_migration/incident_data.json") as f:
    data = json.load(f)

us_state_to_abbrev = {
    "Alabama": "AL",
    "Alaska": "AK",
    "Arizona": "AZ",
    "Arkansas": "AR",
    "California": "CA",
    "Colorado": "CO",
    "Connecticut": "CT",
    "Delaware": "DE",
    "Florida": "FL",
    "Georgia": "GA",
    "Hawaii": "HI",
    "Idaho": "ID",
    "Illinois": "IL",
    "Indiana": "IN",
    "Iowa": "IA",
    "Kansas": "KS",
    "Kentucky": "KY",
    "Louisiana": "LA",
    "Maine": "ME",
    "Maryland": "MD",
    "Massachusetts": "MA",
    "Michigan": "MI",
    "Minnesota": "MN",
    "Mississippi": "MS",
    "Missouri": "MO",
    "Montana": "MT",
    "Nebraska": "NE",
    "Nevada": "NV",
    "New Hampshire": "NH",
    "New Jersey": "NJ",
    "New Mexico": "NM",
    "New York": "NY",
    "North Carolina": "NC",
    "North Dakota": "ND",
    "Ohio": "OH",
    "Oklahoma": "OK",
    "Oregon": "OR",
    "Pennsylvania": "PA",
    "Rhode Island": "RI",
    "South Carolina": "SC",
    "South Dakota": "SD",
    "Tennessee": "TN",
    "Texas": "TX",
    "Utah": "UT",
    "Vermont": "VT",
    "Virginia": "VA",
    "Washington": "WA",
    "West Virginia": "WV",
    "Wisconsin": "WI",
    "Wyoming": "WY",
    "District of Columbia": "DC",
    "American Samoa": "AS",
    "Guam": "GU",
    "Northern Mariana Islands": "MP",
    "Puerto Rico": "PR",
    "United States Minor Outlying Islands": "UM",
    "U.S. Virgin Islands": "VI",
}
# invert the dictionary
abbrev_to_us_state = dict(map(reversed, us_state_to_abbrev.items()))


def add_data():
    lookup_table = {
        (county.name.lower(), county.state.lower()): county
        for county in County.query.all()
    }
    for i, row in enumerate(data):
        month = int(row.get("MONTH"))
        day = int(row.get("DAY"))
        year = int(row.get("CaseYear"))
        minute = int(row.get("MINUTE"))
        hour = int(row.get("HOUR"))

        county_name = (
            row.get("COUNTYNAME").split("(")[0].strip().lower().capitalize() + " County"
        )

        state_name = row.get("STATENAME")
        state_abbrev = us_state_to_abbrev[state_name]
        if state_abbrev.lower() != "tx":
            continue

        lookup_query = (county_name.lower(), state_abbrev.lower())

        matched_county = lookup_table.get(lookup_query)
        if not matched_county:
            continue

        time_period = datetime(year, month, day)
        if minute in range(0, 60) and hour in range(0, 24):
            time_period = time_period.replace(minute=minute, hour=hour)

        incident = TrafficIncident(
            latitude=row.get("LATITUDE"),
            longitude=row.get("LONGITUD"),
            fatalities=row.get("FATALS"),
            drunk_driver=row.get("DRUNK_DR") == "1",
            time_period=time_period,
            num_people=row.get("PERSONS"),
            light_condition=row.get("LGT_CONDNAME"),
            road_type=row.get(""),
            city=row.get("CITYNAME"),
            state=row.get("STATENAME"),
            county=matched_county,
        )
        db.session.add(incident)
    db.session.commit()
