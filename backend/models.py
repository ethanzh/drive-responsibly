from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

"""
incidents = db.Table(
    "incidents",
    db.Column("brewery_id", db.Integer, db.ForeignKey("brewery.id")),
    db.Column("traffic_incident_id", db.Integer, db.ForeignKey("traffic_incident.id")),
)
"""


class County(db.Model):
    id = db.Column(db.String(80), primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    median_household_income = db.Column(db.Integer, nullable=True)
    avg_edu_hs_attain = db.Column(db.Float, nullable=True)
    avg_edu_bach_attain = db.Column(db.Float, nullable=True)
    unemployment = db.Column(db.Float, nullable=True)
    poverty = db.Column(db.Integer, nullable=True)
    population = db.Column(db.Integer, nullable=True)
    latitude = db.Column(db.Float, nullable=True)
    longitude = db.Column(db.Float, nullable=True)
    state = db.Column(db.String(120), nullable=True)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return "<County %r>" % self.name


class Brewery(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    brewery_type = db.Column(db.String(120), nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    address = db.Column(db.String(120), nullable=True)
    county_id = db.Column(db.ForeignKey("county.id"), nullable=False)
    city = db.Column(db.String(120), nullable=True)  # False
    state = db.Column(db.String(120), nullable=True)
    phone = db.Column(db.String(120), nullable=True)
    website = db.Column(db.String(120), nullable=True)
    url = db.Column(db.String(120), nullable=True)
    """
    incidents = db.relationship(
        "TrafficIncident",
        secondary=incidents,
        backref=db.backref("breweries", lazy="dynamic"),
    )
    """

    county = db.relationship(
        "County", backref=db.backref("breweries", lazy=True))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return "<Brewery %r>" % self.name


class TrafficIncident(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    county_id = db.Column(db.ForeignKey("county.id"), nullable=False)
    latitude = db.Column(db.Float, nullable=True)  # False
    longitude = db.Column(db.Float, nullable=True)  # False
    fatalities = db.Column(db.Integer, nullable=True)
    drunk_driver = db.Column(db.Boolean, nullable=True)
    num_people = db.Column(db.Integer, nullable=True)
    time_period = db.Column(db.DateTime, nullable=True)
    light_condition = db.Column(db.String(120), nullable=True)
    road_type = db.Column(db.String(120), nullable=True)
    city = db.Column(db.String(120), nullable=True)
    state = db.Column(db.String(120), nullable=True)

    county = db.relationship(
        "County", backref=db.backref("incidents", lazy=True))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return '<Traffic Incident %r>' % self.id


class BreweryTrafficIncident(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    brewery_id = db.Column(db.ForeignKey("brewery.id"), nullable=False)
    incident_id = db.Column(db.ForeignKey(
        "traffic_incident.id"), nullable=False)
    distance = db.Column(db.Float, nullable=False)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    # brewery = db.relationship(
    #     "Brewery", backref=db.backref("incidents", lazy=True))
    # incident = db.relationship(
    #     "TrafficIncident", backref=db.backref("breweries", lazy=True))
