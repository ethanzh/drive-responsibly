import requests
import json

data = []
page = 1
PAGE_SIZE = 50
while True:
    res = requests.get(
        "https://api.openbrewerydb.org/breweries",
        params={
            "by_state": "texas",
            "per_page": PAGE_SIZE,
            "page": page
        }
    ).json()
    if len(res) == 0:
        break
    print(len(res), res[0])
    data.extend(res)
    page += 1

# with open('breweries.json', 'w') as f:
#     json.dump(data, f)
