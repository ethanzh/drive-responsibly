import unittest
import requests

# BASE_URL = "http://localhost:5000/api"

BASE_URL = "https://api.driveresponsibly.me/api"
headers = {
    "Content-Type": "application/vnd.api+json",
    "Accept": "application/vnd.api+json"
}


class APITests(unittest.TestCase):

    # /traffic_incident
    def test_get_incidents(self):
        ROUTE = "/traffic_incident"
        expected_data_length = 12
        expected_total_length = 13140
        all_data = requests.get(f"{BASE_URL}{ROUTE}", headers=headers).json()
        self.assertEqual(len(all_data["data"]), expected_data_length)
        self.assertEqual(all_data["meta"]["total"], expected_total_length)

    # /traffic_incident

    def test_get_incidents_fields(self):
        ROUTE = "/traffic_incident"
        expected_fields = ['city', 'drunk_driver', 'fatalities', 'latitude',
                           'light_condition', 'longitude', 'num_people', 'road_type', 'state', 'time_period']
        all_data = requests.get(f"{BASE_URL}{ROUTE}", headers=headers).json()
        # print(all_data["data"][0]["attributes"].keys())
        for data in all_data["data"]:
            keys = list(data["attributes"].keys())
            self.assertEqual(keys, expected_fields)

    # /traffic_incident/:id
    def test_get_incident(self):
        ID = 10000
        ROUTE = f"/traffic_incident/{ID}"
        data = requests.get(f"{BASE_URL}{ROUTE}",
                            headers=headers).json()["data"]
        self.assertEqual(data['id'], str(ID))
        self.assertEqual(data['attributes']['time_period'],
                         '2015-01-09T07:32:00')

    # /county
    def test_get_counties(self):
        ROUTE = "/county"
        expected_data_length = 10
        expected_total_length = 254
        all_data = requests.get(f"{BASE_URL}{ROUTE}", headers=headers).json()
        self.assertEqual(len(all_data["data"]), expected_data_length)
        self.assertEqual(all_data["meta"]["total"], expected_total_length)

    # /county
    def test_get_counties_fields(self):
        ROUTE = "/county"
        expected_fields = ['avg_edu_bach_attain', 'avg_edu_hs_attain', 'latitude', 'longitude',
                           'median_household_income', 'name', 'population', 'poverty', 'state', 'unemployment']
        all_data = requests.get(f"{BASE_URL}{ROUTE}", headers=headers).json()
        # print("keys", all_data["data"][0]["attributes"].keys())
        for data in all_data["data"]:
            keys = list(data["attributes"].keys())
            self.assertEqual(keys, expected_fields)

    # /county/:id
    def test_get_county(self):
        ID = 48001
        ROUTE = f"/county/{ID}"
        data = requests.get(f"{BASE_URL}{ROUTE}",
                            headers=headers).json()["data"]

        self.assertEqual(data['id'], str(ID))
        self.assertEqual(data['attributes']['name'],
                         'Anderson County')

    # /brewery
    def test_get_breweries(self):
        ROUTE = "/brewery"
        expected_data_length = 12
        expected_total_length = 169
        all_data = requests.get(f"{BASE_URL}{ROUTE}", headers=headers).json()
        self.assertEqual(len(all_data["data"]), expected_data_length)
        self.assertEqual(all_data["meta"]["total"], expected_total_length)

    # /brewery
    def test_get_breweries_fields(self):
        ROUTE = "/brewery"
        expected_fields = ['address', 'brewery_type', 'city', 'latitude',
                           'longitude', 'name', 'phone', 'state', 'url', 'website']
        all_data = requests.get(f"{BASE_URL}{ROUTE}", headers=headers).json()
        # print("keys", all_data["data"][0]["attributes"].keys())
        for data in all_data["data"]:
            keys = list(data["attributes"].keys())
            self.assertEqual(keys, expected_fields)

    # /brewery/:id
    def test_get_brewery(self):
        ID = 1
        ROUTE = f"/brewery/{ID}"
        data = requests.get(f"{BASE_URL}{ROUTE}",
                            headers=headers).json()["data"]
        self.assertEqual(data['id'], str(ID))
        self.assertEqual(data['attributes']['name'],
                         '3 Nations Brewing')

    def test_nearest_incidents(self):
        ID = 1
        ROUTE = f"/nearby_incidents/{ID}"
        expected_near_neighbors = 5
        data = requests.get(f"{BASE_URL}{ROUTE}",
                            headers=headers).json()
        self.assertEqual(len(data), expected_near_neighbors)
        self.assertEqual(data[0]["id"], 6)

    def test_nearest_breweries(self):
        ID = 10000
        ROUTE = f"/nearby_breweries/{ID}"
        expected_near_neighbors = 1
        data = requests.get(f"{BASE_URL}{ROUTE}",
                            headers=headers).json()
        self.assertEqual(len(data), expected_near_neighbors)
        self.assertEqual(data[0]["id"], 10354)


if __name__ == "__main__":
    # with app.app.app_context():
    unittest.main()
