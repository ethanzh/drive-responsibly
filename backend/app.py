from flask import Flask, request, jsonify
import flask_restless
from models import db, County, Brewery, TrafficIncident, BreweryTrafficIncident
import random
#from data_migration.county import add_data as add_county_data
#from data_migration.traffic_incident import add_data as add_incident_data
#from data_migration.brewery import add_data as add_brewery_data
from haversine import haversine
import pymysql
pymysql.install_as_MySQLdb()


# need to run the following commands:
# pip3 install python-dotenv
# pip3 freeze > requirements.txt
import os
from dotenv import load_dotenv
# load environ variables from '.env' file
load_dotenv()
uri_string = "mysql://" + os.getenv("URL")

app = Flask(__name__)
# mysql://username:password@server/db
# app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////tmp/test.db"
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://admin:qbrxjK7byWsJng8WLEEw@drive-responsibly.cs6dpwuqfgzz.us-east-2.rds.amazonaws.com/rdsdatabase"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.config["CORS_ENABLED"] = False
db.init_app(app)


def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    # Set whatever other headers you like...
    return response


@app.route("/")
def hello_world():
    return "<p>Hello, World! I was automatically deployed!</p>"


@app.route("/api/nearby_breweries/<_id>")
def nearby_breweries(_id):
    res = BreweryTrafficIncident.query.filter_by(
        incident_id=_id).order_by(BreweryTrafficIncident.distance.asc()).limit(5).all()
    print(res, len(res))
    response = jsonify([bti.as_dict() for bti in res])
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Credentials", "true")
    return response


@app.route("/api/nearby_incidents/<_id>")
def nearby_incidents(_id):
    res = BreweryTrafficIncident.query.filter_by(
        brewery_id=_id).order_by(BreweryTrafficIncident.distance.asc()).limit(5).all()
    print(res, len(res), _id)
    response = jsonify([bti.as_dict() for bti in res])
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Credentials", "true")
    return response


@app.route("/api/breweries_in_county/<_id>")
def breweries_in_county(_id):
    res = db.session.query(Brewery).filter_by(county_id=_id).limit(5).all()
    print(res, len(res), _id)
    response = jsonify([bti.as_dict() for bti in res])
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Credentials", "true")
    return response


@app.route("/api/incidents_in_county/<_id>")
def incidents_in_county(_id):
    limit = request.args.get('limit')
    if limit:
        res = db.session.query(TrafficIncident).filter_by(county_id=_id).limit(int(limit)).all()
    else:
        res = db.session.query(TrafficIncident).filter_by(county_id=_id).all()
    # print(res, len(res), _id)
    print(_id)
    response = jsonify([bti.as_dict() for bti in res])
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Credentials", "true")
    return response


"""
def upload_db():
    print("Dropping all tables...")
    db.drop_all()
    print("Creating all tables...")
    db.create_all()
    print("Adding county data...")
    add_county_data()
    print("Adding traffic incident data...")
    add_incident_data()
    print("Adding brewery data...")
    add_brewery_data()

    traffic_incidents = TrafficIncident.query.all()

    for brewery in Brewery.query.all():

        brewery_lat = brewery.latitude
        brewery_lng = brewery.longitude

        if not brewery_lat or brewery_lat == 0 or not brewery_lng or brewery_lng == 0:
            # print("skip", brewery.id)
            continue

        def compare_distance(traffic_incident):
            return haversine((brewery_lat, brewery_lng), (traffic_incident.latitude, traffic_incident.longitude))
        closest_incidents = sorted(traffic_incidents, key=compare_distance)

        for incident in closest_incidents[:5]:
            distance = haversine((brewery_lat, brewery_lng),
                                 (incident.latitude, incident.longitude))
            bti = BreweryTrafficIncident(
                # brewery=brewery
                # incident=incident,
                brewery_id=brewery.id,
                incident_id=incident.id,
                distance=distance
            )

    print("adding missing connections")
    for incident in traffic_incidents:
        cons = BreweryTrafficIncident.query.filter_by(
            incident_id=incident.id).all()
        if len(cons) > 0 or not incident.latitude or not incident.longitude:
            continue

        print(incident)

        def compare_distance(brewery):
            return haversine((brewery.latitude, brewery.longitude), (incident.latitude, incident.longitude))
        closest_breweries = sorted(Brewery.query.all(), key=compare_distance)
        for brewery in closest_breweries[:1]:
            distance = haversine((brewery.latitude, brewery.longitude),
                                 (incident.latitude, incident.longitude))
            bti = BreweryTrafficIncident(
                brewery_id=brewery.id,
                incident_id=incident.id,
                distance=distance
            )

    db.session.commit()

"""

with app.app_context():
    manager = flask_restless.APIManager(app, session=db.session)
    # manager.create_api(County, methods=["GET"])
    # manager.create_api(Brewery, methods=["GET"], exclude=["county"])
    # manager.create_api(TrafficIncident, methods=["GET"], exclude=["county", "brewery"])
    manager.create_api(BreweryTrafficIncident, methods=["GET"])

    brewery_bp = manager.create_api_blueprint('breweryapi', Brewery, page_size=12)
    county_bp = manager.create_api_blueprint('countyapi', County)
    incident_bp = manager.create_api_blueprint('incidentapi', TrafficIncident, page_size=12)

    brewery_bp.after_request(add_cors_headers)
    county_bp.after_request(add_cors_headers)
    incident_bp.after_request(add_cors_headers)

    app.register_blueprint(brewery_bp)
    app.register_blueprint(county_bp)
    app.register_blueprint(incident_bp)

    # upload_db()

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
