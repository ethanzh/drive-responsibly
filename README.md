# Drive Responsibly

Group Members:

- Musab Abdullah: @musababdullah, ma54447
- Kevin Li: @kxl4126, kl34243
- Ethan Houston: @ethanzh, Ezh87
- Sophia Cespedes: @sophiac1, sac4797
- Matthew Jagen: @matthewjagen, mtj595

Git SHA: a360585c4e839d66bf64766ad2e29e9955ec983e


Phase 1 Project Leader: Ethan Houston

Phase 2 Project Leader: Musab Abdullah

Phase 3 Project Leader: Kevin Li

Phase 4 Project Leader: Sophia Cespedes


GitLab Pipelines: https://gitlab.com/ethanzh/drive-responsibly/-/pipelines

Website: https://www.driveresponsibly.me/

## Phase 1 Estimates (in hours)

| Name      | Estimate | Actual |
| ----------- | ----------- |---|
| Musab | 5  | 5|
| Kevin   | 10 | 10 |
| Ethan | 15 | 12 |
| Sophia | 6 | 6 |
| Matthew | 6 | 4 |

## Phase 2 Estimates (in hours)

| Name      | Estimate | Actual |
| ----------- | ----------- |---|
| Musab | 12  | 20 |
| Kevin   | 15 | 20 |
| Ethan | 10 | 25 |
| Sophia | 10 | 18 |
| Matthew | 10 | 19 |

## Phase 3 Estimates (in hours)

| Name      | Estimate | Actual |
| ----------- | ----------- |---|
| Musab | 15 | 10 |
| Kevin   | 10 | 4 |
| Ethan | 10 | 15 |
| Sophia | 20 | 19 |
| Matthew | 10 | 8 |

## Phase 4 Estimates (in hours)

| Name      | Estimate | Actual |
| ----------- | ----------- |---|
| Musab | 5 | 5 |
| Kevin   | 5 | 7 |
| Ethan | 10 | 10 |
| Sophia | 10 | 9 |
| Matthew | 10 | 10 |

Comments:

- The Twitter OEmbed API is very flaky. Sometimes the tweet is embedded correctly, while sometimes it just shows up as raw text and links.
