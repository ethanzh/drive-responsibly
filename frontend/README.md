# Drive Responsibly

Group Members:
- Musab Abdullah: @musababdullah, ma54447
- Kevin Li: @kxl4126, kl34243
- Ethan Houston: @ethanzh, Ezh87
- Sophia Cespedes: @sophiac1, sac4797
- Matthew Jagen: @matthewjagen, mtj595

GitLab Pipelines: https://gitlab.com/ethanzh/drive-responsibly/-/pipelines

Website: https://www.driveresponsibly.me/

Comments: frontend README 

To run jest tests, run\
``npm test -- --watchAll=false --silent``\
from ./frontend/
