import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import {createMemoryHistory} from 'history'
import { Router, MemoryRouter } from 'react-router-dom';
import { act } from 'react-dom/test-utils'
import '@testing-library/jest-dom'
import {App, Routes} from './App';

test('renders nav bar on home page', () => {
  render(<App />);
  const navBar = screen.getByRole("navigation");
  expect(navBar).toBeInTheDocument();
});

test('renders explore modules buttons on home page', () => {
  render(<App />);
  const breweryButtonText = screen.getByText(/Explore Breweries/i);
  expect(breweryButtonText).toBeInTheDocument();
  const countyButtonText = screen.getByText(/Explore Counties/i);
  expect(countyButtonText).toBeInTheDocument();
  const trafficButtonText = screen.getByText(/Explore Traffic Incidents/i);
  expect(trafficButtonText).toBeInTheDocument();
});

test('renders explore module images', () => {
  render(<App />);
  const images = screen.getAllByRole("img");
  expect(images[0].getAttribute("src")).toEqual("brewery.jpg");
  expect(images[1].getAttribute("src")).toEqual("county.gif");
  expect(images[2].getAttribute("src")).toEqual("incident.jpg");
});

test('about page render content', () => {
  const history = createMemoryHistory();
  history.push('/about');
  render(<Router history={history}><Routes /></Router>);
  // verify page content for about page
  expect(screen.getByText(/about us/i)).toBeInTheDocument();
  expect(screen.getByText(/investigates the relationship between breweries/i)).toBeInTheDocument();
  const images = screen.getAllByRole("img");
  expect(images[0].getAttribute("src")).toEqual("musab.png");
  expect(images[1].getAttribute("src")).toEqual("ethan.jpg");
  expect(images[2].getAttribute("src")).toEqual("kevin.jpg");
  expect(images[3].getAttribute("src")).toEqual("sophia.png");
  expect(images[4].getAttribute("src")).toEqual("matthew.jpg");
  expect(screen.getByText(/repository statistics/i)).toBeInTheDocument();
  expect(screen.getByText(/RESTful APIs/i)).toBeInTheDocument();
  expect(screen.getByText(/Project Links/i)).toBeInTheDocument();
});

test('render Explore Brewery module', () => {
  const history = createMemoryHistory();
  history.push('/breweries');
  render(<Router history={history}><Routes /></Router>);
  // verify page header
  expect(screen.getByText(/Explore Breweries/i)).toBeInTheDocument();
});

test('check Explore Brewery table navigation', () => {
  const history = createMemoryHistory();
  history.push('/breweries');
  render(<Router history={history}><Routes /></Router>);
  // verify we are on explore brewery page
  expect(screen.getByText(/Explore Breweries/i)).toBeInTheDocument();
  // check for table navigation
  expect(screen.getByText(/Previous/i)).toBeInTheDocument();
  expect(screen.getByText(/Next/i)).toBeInTheDocument();
});

test('render Explore County module', () => {
  const history = createMemoryHistory();
  history.push('/counties');
  render(<Router history={history}><Routes /></Router>);
  // verify page header
  expect(screen.getByText(/Explore Counties/i)).toBeInTheDocument();
});

test('check Explore County table navigation', () => {
  const history = createMemoryHistory();
  history.push('/counties');
  render(<Router history={history}><Routes /></Router>);
  // verify we are on explore brewery page
  expect(screen.getByText(/Explore Counties/i)).toBeInTheDocument();
  // check for table navigation
  expect(screen.getByText(/Previous/i)).toBeInTheDocument();
  expect(screen.getByText(/Next/i)).toBeInTheDocument();
});

test('render Explore Incident module', () => {
  const history = createMemoryHistory();
  history.push('/incidents');
  render(<Router history={history}><Routes /></Router>);
  // verify page header
  expect(screen.getByText(/Explore Incidents/i)).toBeInTheDocument();
});

test('check Explore Incident table navigation', () => {
  const history = createMemoryHistory();
  history.push('/incidents');
  render(<Router history={history}><Routes /></Router>);
  // verify we are on explore brewery page
  expect(screen.getByText(/Explore Incidents/i)).toBeInTheDocument();
  // check for table navigation
  expect(screen.getByText(/Previous/i)).toBeInTheDocument();
  expect(screen.getByText(/Next/i)).toBeInTheDocument();
});
