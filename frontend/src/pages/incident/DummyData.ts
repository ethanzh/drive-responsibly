export interface Instance {
  id: string;
  type: number;
  severity: number;
  eventCode: number;
  lat: number;
  lng: number;
  startTime: string;
  endTime: string;
  impacting: boolean;
  shortDesc: string;
  fullDesc: string;
  delayFromFreeFlow: number;
  delayFromTypical: number;
  distance: number;
  iconURL: string;
  parameterizedDescription: ParameterizedDescription;
}

interface ParameterizedDescription {
  crossRoad1?: string;
  crossRoad2?: string;
  direction: string;
  position1?: string;
  position2?: string;
  eventText: string;
  toLocation?: string;
  fromLocation?: string;
  roadName: string;
}

export const dummyData: Instance[] = [
  {
    id: "154126654",
    type: 4,
    severity: 3,
    eventCode: 245,
    lat: 30.26561,
    lng: -97.734177,
    startTime: "2021-10-04T12:49:15",
    endTime: "2021-10-04T13:33:45",
    impacting: true,
    shortDesc: "I-35 S/B: two lanes blocked at 6th St",
    fullDesc: "Two lanes blocked due to accident on I-35 Southbound at 6th St.",
    delayFromFreeFlow: 0.0,
    delayFromTypical: 0.0,
    distance: 0.0,
    iconURL: "http://content.mqcdn.com/mqtraffic/incid_mod.png",
    parameterizedDescription: {
      crossRoad2: "I-35 / 6th St",
      direction: "Southbound",
      position1: "at",
      eventText: "Lanes blocked, accident",
      toLocation: "East Cesar Chavez",
      roadName: "I-35",
    },
  },
  {
    id: "129733151",
    type: 1,
    severity: 3,
    eventCode: 701,
    lat: 30.274691,
    lng: -97.731239,
    startTime: "2020-05-13T07:00:00",
    endTime: "2025-03-05T07:00:00",
    impacting: true,
    shortDesc: "I-35 N/B: entry ramp from E 12th St W/B construction work.",
    fullDesc:
      "Construction work and permanently closed on entry ramp to I-35 Northbound from E 12th St Westbound.",
    delayFromFreeFlow: 0.0,
    delayFromTypical: 0.0,
    distance: 0.3499999940395355,
    iconURL: "http://content.mqcdn.com/mqtraffic/const_mod.png",
    parameterizedDescription: {
      crossRoad2: "E 12th St / I-35",
      direction: "None",
      position1: "at",
      eventText: "Construction work, entry Ramp Permanently Closed",
      toLocation: "Central East Austin",
      roadName: "E 12th St",
    },
  },
  {
    id: "131183977",
    type: 1,
    severity: 1,
    eventCode: 401,
    lat: 30.276911,
    lng: -97.739517,
    startTime: "2020-07-06T04:27:02",
    endTime: "2022-12-31T04:55:00",
    impacting: true,
    shortDesc:
      "Congress Ave: road permanently closed from Martin Luther King Jr Blvd to 15th St",
    fullDesc:
      "Road permanently closed due to new road layout on Congress Ave both ways from Martin Luther King Jr Blvd to 15th St.",
    delayFromFreeFlow: 0.0,
    delayFromTypical: 0.0,
    distance: 0.03999999910593033,
    iconURL: "http://content.mqcdn.com/mqtraffic/const_min.png",
    parameterizedDescription: {
      crossRoad2: "15th St / Congress Ave",
      crossRoad1:
        "Martin Luther King Jr Blvd / Martin Luther King Blvd / Congress Ave",
      position2: "to",
      direction: "both ways",
      position1: "from",
      eventText: "Road permanently closed, new road layout",
      fromLocation: "University Of Texas - Austin",
      toLocation: "Downtown",
      roadName: "Congress Ave",
    },
  },
  {
    id: "154126665",
    type: 4,
    severity: 3,
    eventCode: 646,
    lat: 30.277855,
    lng: -97.729889,
    startTime: "2021-10-04T12:49:51",
    endTime: "2021-10-04T13:34:30",
    impacting: false,
    shortDesc: "I-35 N/B: one lane blocked at Martin Luther King Jr Blvd",
    fullDesc:
      "One lane blocked due to stalled vehicle on I-35 Northbound at Martin Luther King Jr Blvd.",
    delayFromFreeFlow: 0.0,
    delayFromTypical: 0.0,
    distance: 0.0,
    iconURL: "http://content.mqcdn.com/mqtraffic/incid_mod.png",
    parameterizedDescription: {
      crossRoad2: "I-35 / I-35 / Martin Luther King Jr Blvd",
      direction: "Northbound",
      position1: "at",
      eventText: "Lanes blocked, stalled Vehicle",
      toLocation: "Central East Austin",
      roadName: "I-35",
    },
  },
  {
    id: "126500020",
    type: 1,
    severity: 1,
    eventCode: 735,
    lat: 30.28134,
    lng: -97.7313,
    startTime: "2020-02-24T08:56:48",
    endTime: "2022-01-31T18:14:00",
    impacting: true,
    shortDesc: "Robert Dedman Dr: road closed from Red River St to 20th St",
    fullDesc:
      "Road closed due to construction on Robert Dedman Dr both ways from Red River St to 20th St.",
    delayFromFreeFlow: 0.0,
    delayFromTypical: 0.0,
    distance: 0.28999999165534973,
    iconURL: "http://content.mqcdn.com/mqtraffic/const_min.png",
    parameterizedDescription: {
      crossRoad2: "Robert Dedman Dr / 20th St",
      crossRoad1: "Red River St / Robert Dedman Dr",
      position2: "to",
      direction: "both ways",
      position1: "from",
      eventText: "Road closed, construction",
      roadName: "Robert Dedman Dr",
    },
  },
  {
    id: "153504137",
    type: 1,
    severity: 0,
    eventCode: 701,
    lat: 30.28652,
    lng: -97.74276,
    startTime: "2021-09-20T06:06:04",
    endTime: "2021-11-05T17:00:00",
    impacting: false,
    shortDesc: "San Antonio St: intermittent lane closures at 23rd St",
    fullDesc:
      "Intermittent lane closures due to long-term construction on San Antonio St both ways at 23rd St.",
    delayFromFreeFlow: 0.0,
    delayFromTypical: 0.0,
    distance: 0.0,
    iconURL: "http://content.mqcdn.com/mqtraffic/const_min.png",
    parameterizedDescription: {
      crossRoad2: "San Antonio St / 23rd St",
      direction: "both ways",
      position1: "at",
      eventText: "Intermittent Lane Closures, long-Term Construction",
      toLocation: "West University",
      roadName: "San Antonio St",
    },
  },
  {
    id: "126498639",
    type: 1,
    severity: 1,
    eventCode: 401,
    lat: 30.2917,
    lng: -97.737747,
    startTime: "2020-02-24T08:39:17",
    endTime: "2021-11-01T19:14:00",
    impacting: false,
    shortDesc: "Wichita St N/B: road closed from Dean Keeton St to 27th St",
    fullDesc:
      "Road closed due to long-term construction on Wichita St Northbound from Dean Keeton St to 27th St.",
    delayFromFreeFlow: 0.0,
    delayFromTypical: 0.0,
    distance: 0.0,
    iconURL: "http://content.mqcdn.com/mqtraffic/const_min.png",
    parameterizedDescription: {
      crossRoad2: "Wichita St / 27th St",
      crossRoad1: "Dean Keeton St / Wichita St",
      position2: "to",
      direction: "Northbound",
      position1: "from",
      eventText: "Road closed, long-Term Construction",
      roadName: "Wichita St",
    },
  },
];
