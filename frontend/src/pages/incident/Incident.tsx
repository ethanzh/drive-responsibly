import React, { useEffect, useState } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { RouteComponentProps } from "react-router";
import { transpileModule } from "typescript";
import GoogleMap from "../../components/GoogleMap";
import TwitterFeed from "../../components/TwitterFeed";
import { CountyConnection } from "../../components/CountyConnection";
import { makeAPIRequest, getTweets, getMap } from "../../utilities/api.js";

const INCIDENT_KEYS_TO_DISPLAY = [
  {label: "city", value: "City"},
  {label: "drunk_driver", value: "Drunk Driver"},
  {label: "fatalities", value: "Fatalities"},
  {label: "latitude", value: "Latitude"},
  {label: "longitude", value: "Longitude"},
  {label: "num_people", value: "Number of People"},
  {label: "road_type", value: "Type of Road"},
  {label: "state", value: "State"},
  {label: "time_period", value: "Time Perid"}
];

const NearbyBreweries = ({ incident_id }: { incident_id: number })=> {
  const [breweries, setBreweries] = useState<any[]>()
  useEffect(() => {
    async function getBreweryData() {
      var { data } = await makeAPIRequest(`nearby_breweries/${incident_id}`, {});
      setBreweries(data);
      console.log('found breweries', data);
    }
    getBreweryData();
  }, [])

  return breweries ? (
    <>
      <h4>Nearby Breweries</h4>
      <ul>
        {breweries.map((brewery: any) => (
          <li key={brewery.id}>
            <a href={`#/breweries/${brewery.brewery_id}`}>Brewery #{brewery.id}</a>
          </li>
        ))}
      </ul>
    </>
  ): (
    <p>Looking for Incidents nearby</p>
  )
}

interface MatchParams {
  id: string;
}

export default function Incident(props: RouteComponentProps<MatchParams>) {
  const [loading, setLoading] = useState(false);
  const [incidentData, setIncidentData] = useState<any>();
  const [tweets, setTweets] = useState<any[]>([]);
  const [map, setMap] = useState<any>();

  useEffect(() => {
    async function getIncidentData() {
      setLoading(true);
      var { data } = await makeAPIRequest(
        `traffic_incident/${props.match.params.id}`,
        {}
      );
      setIncidentData(data.data);
      console.log(data);
      var queries: any[] = [];
      data = data.data
      const city =
        data.attributes.city == "NOT APPLICABLE" ? "" : data.attributes.city;
      if (data.attributes.drunk_driver) {
        queries.push({
          query: `${city} drunk car accident`,
        });

        queries.push({
          query: `${city} drunk accident`,
        });
      }
      queries.push({
        query: `${city} car accident`,
      });

      queries.push({
        query: `${city} accident`,
      });

      if (data.attributes.drunk_driver) {
        queries.push({
          query: `drunk car accident`,
        });
        queries.push({
          query: `drunk accident`,
        });
      }

      queries.push({
        query: `${city} car accident`,
      });

      queries.push({
        query: `${city} accident`,
      });

      queries.push({
        query: `${data.attributes.state} car accident`,
      });

      queries.push({
        query: `${data.attributes.state} accident`,
      });

      const embedTweets = await getTweets(queries);
      setTweets(embedTweets);

      const mapRes = await getMap(
        data.attributes.latitude,
        data.attributes.longitude,
        17
      );
      setMap(mapRes);
      setLoading(false);
    }
    getIncidentData();
  }, [props.match.params.id]);

  return !incidentData ? (
    <h1>loading</h1>
  ) : (
    <Container>
      <Card className="mt-4">
        <Card.Header>
          {incidentData.attributes.name
            ? incidentData.attributes.name
            : `Incident ${props.match.params.id}`}
        </Card.Header>
        <Card.Body>
          {/* <Card.Img variant="top" src={data?.image}></Card.Img> */}
          { incidentData &&
            <Card.Text key={incidentData.id + "city"}>
                {"City"}: {(incidentData.attributes as any)["city"] || "N/A"}
            </Card.Text>
          }
          { incidentData &&
            <Card.Text key={incidentData.id + "drunk_driver"}>
                {"Drunk Driver"}: {(incidentData.attributes as any)["drunk_driver"] || "N/A"}
            </Card.Text>
          }
          { incidentData &&
            <Card.Text key={incidentData.id + "fatalities"}>
                {"Fatalities"}: {(incidentData.attributes as any)["fatalities"] || "N/A"}
            </Card.Text>
          }
          { incidentData &&
            <Card.Text key={incidentData.id + "latitude"}>
                {"Latitude"}: {(incidentData.attributes as any)["latitude"] || "N/A"}
            </Card.Text>
          }
          { incidentData &&
            <Card.Text key={incidentData.id + "longitude"}>
                {"Longitude"}: {(incidentData.attributes as any)["longitude"] || "N/A"}
            </Card.Text>
          }
          { incidentData &&
            <Card.Text key={incidentData.id + "light_condition"}>
                {"Light Condition"}: {(incidentData.attributes as any)["light_condition"] || "N/A"}
            </Card.Text>
          }
          { incidentData &&
            <Card.Text key={incidentData.id + "num_people"}>
                {"Number of People"}: {(incidentData.attributes as any)["num_people"] || "N/A"}
            </Card.Text>
          }
          { incidentData &&
            <Card.Text key={incidentData.id + "road_type"}>
                {"Road Type"}: {(incidentData.attributes as any)["road_type"] || "N/A"}
            </Card.Text>
          }
          { incidentData &&
            <Card.Text key={incidentData.id + "state"}>
                {"State"}: {(incidentData.attributes as any)["state"] || "N/A"}
            </Card.Text>
          }
          { incidentData &&
            <Card.Text key={incidentData.id + "time_period"}>
                {"Time Period"}: {(incidentData.attributes as any)["time_period"] || "N/A"}
            </Card.Text>
          }
          <CountyConnection county_id={incidentData.relationships.county.data.id} />
          <NearbyBreweries incident_id={parseInt(props.match.params.id)}/>
        </Card.Body>
      </Card>
      <Row>
        <Col>
          <GoogleMap src={map} />
        </Col>
        <Col>
          <TwitterFeed tweets={tweets} />
        </Col>
      </Row>
    </Container>
  );
}

/*
{incidentData &&
            INCIDENT_KEYS_TO_DISPLAY.map((key, index) => (
              <Card.Text key={index}>
                {key}: {(incidentData.attributes as any)[key] || "N/A"}
              </Card.Text>
            ))}
*/

