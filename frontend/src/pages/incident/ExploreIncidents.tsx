import React, { useEffect, useState } from "react";
import DataGrid from "react-data-grid";
import { Container, Row, Col, Card, ButtonGroup, Dropdown, DropdownButton, Form, Pagination } from "react-bootstrap";
import { Link, useHistory, useLocation } from "react-router-dom";
import { makeAPIRequest } from "../../utilities/api.js";

const INCIDENT_KEYS_TO_DISPLAY = [
  {label: "id", value: "ID"},
  {label: "city", value: "City"},
  {label: "drunk_driver", value: "Drunk Driver"},
  {label: "fatalities", value: "Fatalities"},
  {label: "latitude", value: "Latitude"},
  {label: "longitude", value: "Longitude"},
  {label: "light_condition", value: "Light Condition"}
];


const INCIDENT_SORTING_FUNCS = [
  {display: "city asc.", url: "city"},
  {display: "city desc.", url: "-city"},
  {display: "fatalities asc.", url: "fatalities"},
  {display: "fatalities desc.", url: "-fatalities"},
]

const LIGHT_CONDITIONS = ["daylight", "dark - lighted", "dark - not lighted", "dusk", "dawn"]

function getHighlightedText(text: string, highlight: string) {
  if (text === null || !text) {
    return text
  }
  if (text.split === undefined) {
    return text
  }
  // Split on highlight term and include term into parts, ignore case
  const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
  return <span> { parts.map((part, i) => 
      <span key={i} style={part.toLowerCase() === highlight.toLowerCase() ? { fontWeight: 'bold' } : {} }>
          { part }
      </span>)
  } </span>;
}

const IncidentsTable = ({
  query,
  incidents,
  handleCardClick,
}: {
  query: any
  incidents: any;
  handleCardClick: any;
}) => {

  function matchesQuery(element: any, index: any, array: any) { 
    return element?.attributes?.city?.toLowerCase()?.includes(query.toLowerCase()) ||
      element?.attributes?.drunk_driver ? "True" : "False".toLowerCase()?.includes(query.toLowerCase()) ||
      element?.attributes?.light_condition?.toLowerCase()?.includes(query.toLowerCase())
  } 

  if (!incidents) return null;
  const columns = [...INCIDENT_KEYS_TO_DISPLAY].map((key) => {
    return { key: key.label, name: key.value };
  });
  const rows = incidents.filter(matchesQuery).map((incident: any) => {
    return {
      id: incident.id,
      city: getHighlightedText(incident.attributes.city, query),
      drunk_driver: getHighlightedText(incident.attributes.drunk_driver ? "True" : "False", query),
      fatalities: incident.attributes.fatalities,
      latitude: incident.attributes.latitude,
      longitude: incident.attributes.longitude,
      light_condition: getHighlightedText(incident.attributes.light_condition, query),
    };
  });
  return (
    <DataGrid
      columns={columns}
      rows={rows}
      onRowClick={handleCardClick}
      className="mt-4"
    />
  );
};

export default function ExploreIncidents() {
  const history = useHistory();
  const search = useLocation().search;
  const page = parseInt(new URLSearchParams(search).get('page') ?? "1") 

  const [pageCount, setPageCount] = useState(1);
  const [incidentsCount, setIncidentsCount] = useState(0);
  const [currentIncidents, setCurrentIncidents] = useState(null);
  const [sorting, setSorting] = useState({display: "city asc.", url: "city"});
  const [filter, setFilter] = useState<any>({});
  const [searchQuery, setSearchQuery] = useState("");

  async function getIncidentsData(pageNumber: number, sort: string, filter: string) {
    const requestParams = {
      "page[number]": pageNumber,
      "sort": sort,
      "filter[objects]": filter
    };
    const { data } = await makeAPIRequest("traffic_incident", requestParams);
    setCurrentIncidents(data.data);
    setIncidentsCount(data.meta.total);
    setPageCount(Math.floor(data.meta.total / 12));
  }

  useEffect(() => {
    console.log("running use effect", page, sorting.url, filter)
    var filter_string = ""
    if (Object.keys(filter).length === 1) {
      filter_string = JSON.stringify(Object.values(filter))
    } else if (Object.keys(filter).length > 1) {
      filter_string = JSON.stringify([{"and": Object.values(filter)}])
    }
    getIncidentsData(page, sorting.url, filter_string);
  }, [page, sorting, filter]);

  function handleCardClick(row: any) {
    history.push(`/incidents/${row.id}`);
  }

  const goToPage = (number: number) => {
    history.push(`/incidents?page=${number}`)
  }

  function handleQueryChange(event: any) {
    setSearchQuery(event.target.value)
  }

  return (
    <Container fluid className="mt-4">
      <h2 className="text-center mt-3">Explore Incidents</h2>
      <h4>Found {incidentsCount || 0} matching results</h4>

      <Row>
        <Col style={{display: 'flex', justifyContent: 'center'}}>
          <DropdownButton id="sort-dropdown" title={`Sort By: ${sorting.display}`}>
            {INCIDENT_SORTING_FUNCS.map((sort, index) => (
              <Dropdown.Item key={index} onClick={() => setSorting(sort)}>{sort.display}</Dropdown.Item>
            ))}
          </DropdownButton>
        </Col>
        <Col style={{display: 'flex', justifyContent: 'center'}}>
          <input type="text" value={searchQuery || ''} onChange={handleQueryChange} />
        </Col>
        <Col style={{display: 'flex', justifyContent: 'center'}}>
          <DropdownButton as={ButtonGroup} title="drunk driver">
          <Dropdown.Item active={"drunk_driver" in filter && filter['drunk_driver'].val} onClick={() => setFilter({...filter, drunk_driver: {"name":"drunk_driver","op":"==","val":true}})}>yes</Dropdown.Item>
          <Dropdown.Item active={"drunk_driver" in filter && !filter['drunk_driver'].val} onClick={() => setFilter({...filter, drunk_driver: {"name":"drunk_driver","op":"==","val":false}})}>no</Dropdown.Item>
          </DropdownButton>

          <DropdownButton as={ButtonGroup} title="light conditions">
            {LIGHT_CONDITIONS.map(condition => (
              <Dropdown.Item active={"light_condition" in filter && filter['light_condition'].val === condition} key={condition} onClick={() => setFilter({...filter, light_condition: {"name":"light_condition","op":"==","val":condition}})}>{condition}</Dropdown.Item>
            ))}
          </DropdownButton>

          <DropdownButton as={ButtonGroup} title="area">
            <Dropdown.Item active={"city" in filter && filter['city'].op === "!=" } onClick={() => setFilter({...filter, city: {"name":"city","op":"!=","val":"NOT APPLICABLE"}})}>urban</Dropdown.Item>
            <Dropdown.Item active={"city" in filter && filter['city'].op === "==" } onClick={() => setFilter({...filter, city: {"name":"city","op":"==","val":"NOT APPLICABLE"}})}>rural</Dropdown.Item>
          </DropdownButton>
        </Col>
      </Row>

      <IncidentsTable
        query={searchQuery}
        incidents={currentIncidents}
        handleCardClick={handleCardClick}
      />
      <br/>
      <Pagination size='lg' style={{display: 'flex', justifyContent: 'center'}}>
        <Pagination.First onClick={() => goToPage(1)} />
        <Pagination.Prev onClick={() => goToPage(Math.max(page - 1, 0))} />
        <Pagination.Item onClick={() => goToPage(page)} >{page}</Pagination.Item>
        <Pagination.Next onClick={() => goToPage(Math.min(page + 1, pageCount + 1))}/>
        <Pagination.Last onClick={() => goToPage(pageCount + 1)}  />
      </Pagination>
    </Container>
  );
}
