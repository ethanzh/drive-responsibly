import React, { useEffect, useState } from "react";
import { Card, Container } from "react-bootstrap";
import { RouteComponentProps } from "react-router";
import GoogleMap from "../../components/GoogleMap";
import TwitterFeed from "../../components/TwitterFeed";
import { makeAPIRequest, getTweets, getMap } from "../../utilities/api.js";

const BreweriesInCounty = ({ county_id }: { county_id: number }) => {
  const [breweries, setBreweries] = useState<any[]>();
  useEffect(() => {
    async function getBreweryData() {
      var { data } = await makeAPIRequest(
        `breweries_in_county/${county_id}`,
        {}
      );
      setBreweries(data);
      console.log("found breweries", data);
    }
    getBreweryData();
  }, []);

  return breweries && breweries.length > 0 ? (
    <>
      <h4>Breweries In County</h4>
      <ul>
        {breweries.map((brewery: any) => (
          <li key={brewery.id}>
            <a href={`#/breweries/${brewery.id}`}>{brewery.name}</a>
          </li>
        ))}
        {}
      </ul>
    </>
  ) : (
    <p>No Breweries in County</p>
  );
};

const IncidentsInCounty = ({ county_id }: { county_id: number }) => {
  const [incidents, setIncidents] = useState<any[]>();
  useEffect(() => {
    async function getIncidentData() {
      const requestParams = {
        "limit": 5
      };
      var { data } = await makeAPIRequest(
        `incidents_in_county/${county_id}`,
        requestParams
      );
      setIncidents(data);
      console.log("found incidents", data);
    }
    getIncidentData();
  }, []);

  return incidents && incidents.length > 0 ? (
    <>
      <h4>Incidents In County</h4>
      <ul>
        {incidents.map((incident: any) => (
          <li key={incident.id}>
            <a href={`#/incidents/${incident.id}`}>{incident.id}</a>
          </li>
        ))}
      </ul>
    </>
  ) : (
    <p>No Incidents in County</p>
  );
};

interface MatchParams {
  id: string;
}

const COUNTY_KEYS_TO_DISPLAY = [
  "population",
  "poverty",
  "unemployment",
  "avg_edu_bach_attain",
  "median_household_income",
  "avg_edu_hs_attain",
  // "latitude",
  // "longitude",
];

export default function County(props: RouteComponentProps<MatchParams>) {
  const [loading, setLoading] = useState(false);
  const [countyData, setCountyData] = useState<any>();
  const [tweets, setTweets] = useState<any>([]);
  const [map, setMap] = useState<any>();

  useEffect(() => {
    async function getCountyData() {
      setLoading(true);
      var { data } = await makeAPIRequest(
        `county/${props.match.params.id}`,
        {}
      );
      setCountyData(data.data);
      data = data.data;
      console.log(data.attributes.name);
      const embedTweets = await getTweets([
        {
          query: `${data.attributes.name} County`,
        },
      ]);
      console.log("tweets", embedTweets);
      setTweets(embedTweets);
      const mapRes = await getMap(data.attributes.name, "Texas", 8);
      setMap(mapRes);
      setLoading(false);
    }
    getCountyData();
  }, [props.match.params.id]);

  return !countyData ? (
    <h1>loading</h1>
  ) : (
    <Container>
      <Card className="mt-4">
        <Card.Header>{countyData.attributes.name}</Card.Header>
        <Card.Body>
          {/* <Card.Img variant="top" src={data?.image}></Card.Img> */}
          { countyData &&
            <Card.Text key={countyData.id + "population"}>
                {"Population"}: {((countyData.attributes as any)["population"]).toLocaleString() || "N/A"}
            </Card.Text>
          }
          { countyData &&
            <Card.Text key={countyData.id + "poverty"}>
                {"Poverty"}: {((countyData.attributes as any)["poverty"]).toLocaleString() || "N/A"}
            </Card.Text>
          }
          { countyData &&
            <Card.Text key={countyData.id + "unemployment"}>
                {"Unemployment"}: {Number((countyData.attributes as any)["unemployment"]).toFixed(2) || "N/A"}
            </Card.Text>
          }
          { countyData &&
            <Card.Text key={countyData.id + "avg_edu_bach_attain"}>
                {"Average Bachelor's Degree Attainment"}: {Number((countyData.attributes as any)["avg_edu_bach_attain"]).toFixed(2) || "N/A"}
            </Card.Text>
          }
          { countyData &&
            <Card.Text key={countyData.id + "avg_edu_hs_attain"}>
                {"Average High School Educational Attainment"}: {Number((countyData.attributes as any)["avg_edu_hs_attain"]).toFixed(2) || "N/A"}
            </Card.Text>
          }
          { countyData &&
            <Card.Text key={countyData.id + "median_household_income"}>
                {"Median Household Income"}: {((countyData.attributes as any)["median_household_income"]).toLocaleString() || "N/A"}
            </Card.Text>
          }
          <BreweriesInCounty county_id={parseInt(props.match.params.id)} />
          <IncidentsInCounty county_id={parseInt(props.match.params.id)} />
        </Card.Body>
      </Card>
      <GoogleMap src={map} />
      <TwitterFeed tweets={tweets} />
    </Container>
  );
}
