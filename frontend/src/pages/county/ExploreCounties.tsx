import React, { useEffect, useState } from "react";
import { Container, Row, Col, Card, ButtonGroup, Dropdown, DropdownButton, Form, Pagination } from "react-bootstrap";
import {  useHistory, useLocation } from "react-router-dom";

import { makeAPIRequest } from "../../utilities/api.js"

const COUNTY_SORTING_FUNCS = [
  {display: "name asc.", url: "name"},
  {display: "name desc.", url: "-name"},
  {display: "population asc.", url: "population"},
  {display: "population desc.", url: "-population"},
  {display: "poverty asc.", url: "poverty"},
  {display: "poverty desc.", url: "-poverty"},
  {display: "unemployment asc.", url: "unemployment"},
  {display: "unemployment desc.", url: "-unemployment"},
]

function getHighlightedText(text: string, highlight: string) {
  if (text === null || !text) {
    return text
  }
  if (text.split === undefined) {
    return text
  }
  // Split on highlight term and include term into parts, ignore case
  const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
  return <span> { parts.map((part, i) => 
      <span key={i} style={part.toLowerCase() === highlight.toLowerCase() ? { fontWeight: 'bold' } : {} }>
          { part }
      </span>)
  } </span>;
}

// const COUNTY_FILTERS = [
//   {name: "population", options: ["small (<10,000)", "medium (10,000 - 100,000)", "large (>100,000)"]},
// ]

const CountiesGrid = ({ query, counties, handleCardClick }: {query: string, counties: any, handleCardClick: any}) => {

  function matchesQuery(element: any, index: any, array: any) { 
    return element?.attributes?.name?.toLowerCase()?.includes(query.toLowerCase())
  } 
  return counties && (
      <Row>
        {counties.filter(matchesQuery).map((county: any) => {
          let data = county.attributes
          return (
            <Col key={county.id} xs={4}>
              <Card
                className="mt-5"
                onClick={() => handleCardClick(county.id)}
                style={{ cursor: "pointer" }}
              >
                <Card.Header>
                  <Card.Title>{getHighlightedText(data?.name, query)}</Card.Title>
                </Card.Header>
                <Card.Body>
                <Card.Text key={data.id + "population"}>
                      {"Population"}: {(getHighlightedText(((data as any)["population"]).toLocaleString(), query))}
                  </Card.Text>
                  <Card.Text key={data.id + "poverty"}>
                      {"Poverty"}: {(getHighlightedText(((data as any)["poverty"]).toLocaleString(), query))}
                  </Card.Text>
                  <Card.Text key={data.id + "unemployment"}>
                      {"Unemployment"}: {(getHighlightedText(Number((data as any)["unemployment"]).toFixed(2), query))}
                  </Card.Text>
                  <Card.Text key={data.id + "avg_edu_bach_attain"}>
                      {"Average Bachelor's Degree Attainment"}: {(getHighlightedText(Number((data as any)["avg_edu_bach_attain"]).toFixed(2), query))}
                  </Card.Text>
                  <Card.Text key={data.id + "median_household_income"}>
                      {"Median Household Income"}: {(getHighlightedText(((data as any)["median_household_income"]).toLocaleString(), query))}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          );
        })}
      </Row>
  )
}

export default function ExploreCounties() {
  const history = useHistory();
  const search = useLocation().search;
  const page = parseInt(new URLSearchParams(search).get('page') ?? "1") 

  const [pageCount, setPageCount] = useState(1);
  const [countiesCount, setCountiesCount] = useState(0);
  const [currentCounties, setCurrentCounties] = useState(null);
  const [sorting, setSorting] = useState({display: "name asc.", url: "name"});
  const [filter, setFilter] = useState<any>({});
  const [searchQuery, setSearchQuery] = useState("");

  async function getCountiesData(pageNumber: number, sort: string, filter: string) {
    const requestParams = {
      "page[number]": pageNumber,
      "sort": sort,
      "filter[objects]": filter,
      "page[size]": 12
    };
    const { data } = await makeAPIRequest("county", requestParams);
    setCurrentCounties(data.data);
    setCountiesCount(data.meta.total)
    setPageCount(Math.floor(data.meta.total / 12))
  }


  useEffect(() => {
    console.log("running use effect", page, sorting.url, filter)
    var filter_string = ""
    if (Object.keys(filter).length === 1) {
      filter_string = JSON.stringify(Object.values(filter))
    } else if (Object.keys(filter).length > 1) {
      filter_string = JSON.stringify([{"and": Object.values(filter)}])
    }
    getCountiesData(page, sorting.url, filter_string);
  }, [page, sorting, filter]);

  function handleCardClick(id: number) {
    history.push(`/counties/${id}`);
  }

  const goToPage = (number: number) => {
    history.push(`/counties?page=${number}`)
  }


  function handleQueryChange(event: any) {
    setSearchQuery(event.target.value)
  }

  return (
    <Container fluid className="mt-4">
      <h2 className="text-center mt-3">Explore Counties</h2>
      <h4>Found {countiesCount || 0} matching results</h4>
      <Row>
        <Col style={{display: 'flex', justifyContent: 'center'}}>
          <DropdownButton id="sort-dropdown" title={`Sort By: ${sorting.display}`}>
            {COUNTY_SORTING_FUNCS.map((sort, index) => (
              <Dropdown.Item key={index} onClick={() => setSorting(sort)}>{sort.display}</Dropdown.Item>
            ))}
          </DropdownButton>
        </Col>
        <Col style={{display: 'flex', justifyContent: 'center'}}>
          <input type="text" value={searchQuery || ''} onChange={handleQueryChange} />
        </Col>
        <Col style={{display: 'flex', justifyContent: 'center'}}>
          <DropdownButton as={ButtonGroup} title="population">
            <Dropdown.Item active={"population" in filter && filter['population'].op === "lt"} onClick={() => setFilter({...filter, population: {"name":"population","op":"lt","val": 10000}})}>small (&lt; 10,000)</Dropdown.Item>
            <Dropdown.Item active={"population" in filter && "and" in filter['population']} onClick={() => setFilter({...filter, population: {"and":[{"name":"population","op":"lte","val":100000},{"name":"population","op":"gte","val":10000}]}})}>medium (10,000 - 100,000)</Dropdown.Item>
            <Dropdown.Item active={"population" in filter && filter['population'].op === "gt"} onClick={() => setFilter({...filter, population: {"name":"population","op":"gt","val":100000}})}>large (&gt; 100,000)</Dropdown.Item>
          </DropdownButton>

          <DropdownButton as={ButtonGroup} title="poverty">
            <Dropdown.Item active={"poverty" in filter && filter['poverty'].op === "lt"} onClick={() => setFilter({...filter, poverty: {"name":"poverty","op":"lt","val": 1000}})}>small (&lt; 1000)</Dropdown.Item>
            <Dropdown.Item active={"poverty" in filter && "and" in filter['poverty']} onClick={() => setFilter({...filter, poverty: {"and":[{"name":"poverty","op":"lte","val":50000},{"name":"poverty","op":"gte","val":1000}]}})}>medium (1000 - 50,000)</Dropdown.Item>
            <Dropdown.Item active={"poverty" in filter && filter['poverty'].op === "gt"} onClick={() => setFilter({...filter, poverty: {"name":"poverty","op":"gt","val":50000}})}>large (&gt; 50,000)</Dropdown.Item>
          </DropdownButton>
        </Col>
      </Row>
      <CountiesGrid query={searchQuery} counties={currentCounties} handleCardClick={handleCardClick} />
      <br/>
      <Pagination size='lg' style={{display: 'flex', justifyContent: 'center'}}>
        <Pagination.First onClick={() => goToPage(1)} />
        <Pagination.Prev onClick={() => goToPage(Math.max(page - 1, 0))} />
        <Pagination.Item onClick={() => goToPage(page)} >{page}</Pagination.Item>
        <Pagination.Next onClick={() => goToPage(Math.min(page + 1, pageCount + 1))}/>
        <Pagination.Last onClick={() => goToPage(pageCount + 1)}  />
      </Pagination>
    </Container>
  )
}