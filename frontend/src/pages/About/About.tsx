import { useEffect, useState } from "react"
import { tools, apis, musab, ethan, kevin, sophia, matthew } from "./Stuff"
import { Container, Row, Col, Card } from "react-bootstrap"

import { ListGroup } from "react-bootstrap"

const COMMIT_URL = "https://gitlab.com/api/v4/projects/30091483/repository/commits?per_page=1000";
const ISSUE_URL = "https://gitlab.com/api/v4/projects/30091483/issues?per_page=1000";

const numCommits = (email: string, commits: [] | null) => {
  if (!commits) return 0;
  return commits.filter(commit => commit['author_email'] === email).length;
};

const numIssues = (username: string, issues: [] | null) => {
  // count issues that have an assignee and are assigned to this username
  if (!issues) return 0;
  return issues.filter(issue => issue['assignee'] && issue['assignee']['username'] === username).length;
};

const numTests = (username: string, tests: [] | null) => {
  // count tests that have an assignee and are assigned to this username
  if (!tests) return 0
  return tests.filter(test => test['assignee'] && test['assignee']['username'] === username).length
}

interface teamCardInterface {
  name: string;
  email: string;
  role: string;
  image: string;
  bio: string;
  commits: number;
  issues: number;
  tests: number;
}

const TeamCard = ({ name, email, role, image, bio, commits, issues, tests }: teamCardInterface) => (
  <Card style={{width: "18rem" }}>
    <Card.Img variant="top" src={image} />
    <Card.Body>
      <p><b>{name}</b></p>
      <p><i>{role}</i></p>
      <p>{bio}</p>
      <p>commits: {commits}</p>
      <p>issues: {issues}</p>
      <p>tests: {tests}</p>
    </Card.Body>
  </Card>
);

export default function About() {
  const [commits, setCommits] = useState<[] | null>(null);
  const [issues, setIssues] = useState<[] | null>(null);
  const [tests, setTests] = useState<[] | null>(null);
  useEffect(() => {
    fetch(COMMIT_URL)
      .then(res => res.json())
      .then(data => setCommits(data));
    fetch(ISSUE_URL)
      .then(res => res.json())
      .then(data => setIssues(data));
    fetch(COMMIT_URL)
      .then(res => res.json())
      .then(data => setTests(data));
  }, []);

  const NUM_TOOLS = tools.length;

  return (
    <>
    <div className="text-center">
      <h1>About Us</h1>
      <p><b>Drive Responsibly</b> investigates the relationship between breweries, the counties they reside in, and traffic incidents within Texas.</p>
    </div>
    <Container>
      <Row className="justify-content-center" style={{gap: '40px 10px'}}>
        <Col md="auto">
          <TeamCard name={musab.name} email={musab.email} role={musab.role} tests={musab.tests} image={musab.image} bio={musab.bio} commits={numCommits(musab.email, commits)} issues={numIssues(musab.username, issues)} />
        </Col>
        <Col md="auto">
          <TeamCard name={ethan.name} email={ethan.email} role={ethan.role} tests={ethan.tests} image={process.env.PUBLIC_URL + ethan.image} bio={ethan.bio} commits={numCommits(ethan.email, commits)} issues={numIssues(ethan.username, issues)} />
        </Col>
        <Col md="auto">
          <TeamCard name={kevin.name} email={kevin.email} role={kevin.role} tests={kevin.tests} image={kevin.image} bio={kevin.bio} commits={numCommits(kevin.email, commits)} issues={numIssues(kevin.username, issues)} />
        </Col>
        <Col md="auto">
          <TeamCard name={sophia.name} email={sophia.email} role={sophia.role} tests={sophia.tests} image={sophia.image} bio={sophia.bio} commits={numCommits(sophia.email, commits)} issues={numIssues(sophia.username, issues)} />
        </Col>
        <Col md="auto">
          <TeamCard name={matthew.name} email={matthew.email} role={matthew.role} tests={matthew.tests} image={matthew.image} bio={matthew.bio} commits={numCommits(matthew.email, commits)} issues={numIssues(matthew.username, issues)} />
        </Col>
      </Row>
    </Container>

    <Container>
      <Row className="justify-content-md-center">
        <div className="text-center">
          <h2>Repository Statistics</h2>
          <p>
            {commits ? commits.length: '?'} commits |
            {issues ? issues.length : '?'} issues | 
            {tests ? tests.length : '?'} tests
          </p>
        </div>
      </Row>
      <Row className="justify-content-md-center">
        <div className="text-center">
          <h2>Project Links</h2>
          <a href="https://gitlab.com/ethanzh/drive-responsibly">Gitlab Repo</a><br/>
          <a href="https://documenter.getpostman.com/view/10582451/UUy4ckUf">API Documentation</a>
        </div>
      </Row>
    </Container>

    <Container>
      <div className="text-center">
        <h2>Tools</h2>
        <Row>
          <ListGroup horizontal>
            <Col>
              <ListGroup.Item>
                <a href={tools[0].link}><h3>{tools[0].title}</h3></a>
                <img src={tools[0].image} alt={`Image for ${tools[0].title}`} width="200" height="200" />
                <p>{tools[0].description}</p>
              </ListGroup.Item>
            </Col>
            <Col>
              <ListGroup.Item>
                <a href={tools[1].link}><h3>{tools[1].title}</h3></a>
                <img src={tools[1].image} alt={`Image for ${tools[1].title}`} width="200" height="200" />
                <p>{tools[1].description}</p>
              </ListGroup.Item>
            </Col>
            <Col>
              <ListGroup.Item>
                <a href={tools[2].link}><h3>{tools[2].title}</h3></a>
                <img src={tools[2].image} alt={`Image for ${tools[2].title}`} width="200" height="200" />
                <p>{tools[2].description}</p>
              </ListGroup.Item>
            </Col>
          </ListGroup>
        </Row>
        <Row>
          <ListGroup horizontal>
          <Col>
              <ListGroup.Item>
                <a href={tools[3].link}><h3>{tools[3].title}</h3></a>
                <img src={tools[3].image} alt={`Image for ${tools[3].title}`} width="200" height="200" />
                <p>{tools[3].description}</p>
              </ListGroup.Item>
            </Col>
            <Col>
              <ListGroup.Item>
                <a href={tools[4].link}><h3>{tools[4].title}</h3></a>
                <img src={tools[4].image} alt={`Image for ${tools[4].title}`} width="200" height="200" />
                <p>{tools[4].description}</p>
              </ListGroup.Item>
            </Col>
            <Col>
              <ListGroup.Item>
                <a href={tools[5].link}><h3>{tools[5].title}</h3></a>
                <img src={tools[5].image} alt={`Image for ${tools[5].title}`} width="200" height="200" />
                <p>{tools[5].description}</p>
              </ListGroup.Item>
            </Col>
          </ListGroup>
        </Row>
      </div>
    </Container>

    <Container>
      <div className="text-center">
        <h2>RESTful APIs and Supporting Data</h2>
        <Row>
          <ListGroup horizontal>
            <Col>
              <ListGroup.Item>
                <a href={apis[0].link}><h3>{apis[0].title}</h3></a>
                <img src={apis[0].image} alt={`Image for ${apis[0].title}`} width="200" height="200" />
                <p>{apis[0].description}</p>
              </ListGroup.Item>
            </Col>
            <Col>
              <ListGroup.Item>
                <a href={apis[1].link}><h3>{apis[1].title}</h3></a>
                <img src={apis[1].image} alt={`Image for ${apis[1].title}`} width="200" height="200" />
                <p>{apis[1].description}</p>
              </ListGroup.Item>
            </Col>
            <Col>
              <ListGroup.Item>
                <a href={apis[2].link}><h3>{apis[2].title}</h3></a>
                <img src={apis[2].image} alt={`Image for ${apis[2].title}`} width="200" height="200" />
                <p>{apis[2].description}</p>
              </ListGroup.Item>
            </Col>
          </ListGroup>
        </Row>
        <Row>
          <ListGroup horizontal>
          <Col>
              <ListGroup.Item>
                <a href={apis[3].link}><h3>{apis[3].title}</h3></a>
                <img src={apis[3].image} alt={`Image for ${apis[3].title}`} width="200" height="200" />
                <p>{apis[3].description}</p>
              </ListGroup.Item>
            </Col>
            <Col>
              <ListGroup.Item>
                <a href={apis[4].link}><h3>{apis[4].title}</h3></a>
                <img src={apis[4].image} alt={`Image for ${apis[4].title}`} width="200" height="200" />
                <p>{apis[4].description}</p>
              </ListGroup.Item>
            </Col>
          </ListGroup>
        </Row>
      </div>
    </Container>
    </>
  )
};

/*
tools.map((tool, index) => (
        <ListGroup.Item key={index}>
          <a href={tool.link}><h3>{tool.title}</h3></a>
          <img src={tool.image} alt={''} width="200" height="200" />
          <p>{tool.description}</p>
          <footer><p></p></footer>
        </ListGroup.Item>
      ))}

<div className="text-center">
    <h2>RESTful APIs and Supporting Data</h2>
    {apis.map((api, index) => (
      <div key={index}>
        <a href={api.link}><h3>{api.title}</h3></a>
        <img src={api.image} alt={''} width="200" />
        {/* <h3>{api.title}</h3> *//*}
        <p>{api.description}</p>
      </div>
    ))}
    </div>
*/
