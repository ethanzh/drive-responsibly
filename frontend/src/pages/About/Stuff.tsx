import musab_img from './images/musab.png'
import kevin_img from './images/kevin.jpg'
import ethan_img from './images/ethan.jpg'
import sophia_img from './images/sophia.png'
import matthew_img from './images/matthew.jpg'
import react from './images/react.png'
import flask from './images/flask.png'
import postman from './images/postman.png'
import aws from './images/aws.png'
import docker from './images/docker.png'
import gitlab from './images/gitlab.png'
import openbrewery from './images/openbrewery.jpg'
import census from './images/census.jpg'
import traffic from './images/traffic.jpg'
import usda from './images/usda-symbol.svg'
import gmaps from './images/maps-icon.svg'

export const tools = [
  {
    title: 'React',
    image: react,
    description: 'A JavaScript library for building user interfaces',
    link: 'https://reactjs.org/'
  },
  {
    title: 'Flask',
    image: flask,
    description: 'A mini web framework written in Python',
    link: 'https://flask.palletsprojects.com/en/2.0.x/'
  },
  {
    title: "Postman",
    description: "A platform for buildings and testing APIs",
    image: postman,
    link: "https://postman.com/",
  },
  {
    title: "AWS",
    image: aws,
    description: "A comprehensive cloud service platform",
    link: "https://aws.amazon.com/",
  },
  {
    title: "Docker",
    image: docker,
    description: "A containerization tool for consistent runtime environments",
    link: "https://docker.com/",
  },
  {
    title: "GitLab",
    image: gitlab,
    description: "A git repository and CI/CD platform",
    link: "https://gitlab.com/",
  }
]

export const apis = [
  {
    title: "OpenBreweryDB",
    image: openbrewery,
		description: "A free dataset and API with public information on breweries, cideries, brewpubs, and bottleshops",
		link: "https://www.ers.usda.gov/data-products/county-level-data-sets/download-data/",
  },
  {
    title: "Decennial Census API",
    image: census,
		description: "An API for the decennial census conducted by the US Census Bureau",
		link: "https://www.census.gov/data/developers/data-sets/decennial-census.html",
  },
  {
    title: "USDA ERS County-Level Data Sets",
    image: usda,
		description: "Data Sets containing demographic information about different U.S. counties",
		link: "https://www.ers.usda.gov/data-products/county-level-data-sets/download-data/",
  },
  {
    title: "Traffic Incident API",
    image: traffic,
    description: "An API for information on traffic incidents in the US",
    link: "https://developer.mapquest.com/documentation/traffic-api/incidents/get/",
  },
  {
    title: "Google Geocoding API",
    image: gmaps,
    description: "An API with reverse-geocoding capabilities, used to connect locations and latitude/longitude coordinates",
    link: "https://developers.google.com/maps/documentation/geocoding/overview#ReverseGeocoding",
  }
]

export const musab = {
  name: 'Musab Abdullah',
  email: 'musababdullah@utexas.edu',
  username: 'musababdullah',
  role: 'Fullstack Development',
  tests: 0,
  image: musab_img,
  bio: 'Musab is a third year CS student that loves lifting, cooking, and watching Netflix.'
}

export const ethan = {
  name: 'Ethan Houston',
  email: 'ethan.houston@gmail.com',
  username: 'ethanzh',
  role: 'Backend/Infrastructure',
  tests: 0,
  image: ethan_img,
  bio: 'Ethan is a fourth year CS student from Palo Alto interested in databases and programming languages. His hobbies include playing rugby, drinking German beer, running, and breakfast tacos.'
}

export const kevin = {
  name: 'Kevin Li',
  email: 'kevkev00@gmail.com',
  username: 'kxl4126',
  role: 'Fullstack Development',
  tests: 0,
  image: kevin_img,
  bio: 'Kevin is a third year CS student who is interested in full-stack and blockchain development. He enjoys soccer, video games, and traveling.'
}

export const sophia = {
  name: 'Sophia Cespedes',
  email: 'idk@gmail.com',
  username: 'sophiac1',
  role: 'Backend Development',
  tests: 0,
  image: sophia_img,
  bio: 'Sophia Cespedes is a fourth-year CS student who is interested in backend development and exploring new topics. She enjoys Brazilian Jiu Jitsu, practicing pieces on the piano and playing video games.'
}

export const matthew = {
  name: 'Matthew Jagen',
  email: 'matthewjagent@gmail.com',
  username: 'matthewjagen',
  role: 'Fullstack Development',
  tests: 0,
  image: matthew_img,
  bio: 'Matthew is a third year CS student who is interested in computer graphics and virtual reality. He enjoys music and video games.'
}