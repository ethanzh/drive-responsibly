import React, { useEffect, useState } from "react";
import { Container, Row, Col, Card, ButtonGroup, DropdownButton, Dropdown, Form, Pagination, ToggleButton } from "react-bootstrap";
import {useLocation} from "react-router-dom";
import { Link, useHistory } from "react-router-dom";
import { makeAPIRequest } from "../../utilities/api.js"

const BREWERY_SORTING_FUNCS = [
  {display: "name asc.", url: "name"},
  {display: "name desc.", url: "-name"},
  {display: "city asc.", url: "city"},
  {display: "city desc.", url: "-city"},
]

const BREWERY_TYPES = ["micro", "regional", "brewpub", "large", "contract", "closed"]

function getHighlightedText(text: string, highlight: string) {
  if (text === null || !text) {
    return text
  }
  if (text.split === undefined) {
    return text
  }
  // Split on highlight term and include term into parts, ignore case
  const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
  return <span> { parts.map((part, i) => 
      <span key={i} style={part.toLowerCase() === highlight.toLowerCase() ? { fontWeight: 'bold' } : {} }>
          { part }
      </span>)
  } </span>;
}

const BreweriesGrid = ({ query, breweries, handleCardClick }: {query: string, breweries: any, handleCardClick: any}) => {
  function matchesQuery(element: any, index: any, array: any) { 
    return element?.attributes?.name?.toLowerCase()?.includes(query.toLowerCase()) ||
    element?.attributes?.city?.toLowerCase()?.includes(query.toLowerCase()) ||
    element?.attributes?.brewery_type?.toLowerCase()?.includes(query.toLowerCase()) ||
    element?.attributes?.website?.toLowerCase()?.includes(query.toLowerCase())
  } 

  return breweries && (
      <Row>
        {breweries.filter(matchesQuery).map((brewery: any) => {
          let data = brewery.attributes
          return (
            <Col key={brewery.id} xs={4}>
              <Card
                className="mt-5"
                onClick={() => handleCardClick(brewery.id)}
                style={{ cursor: "pointer" }}
              >
                <Card.Header>
                  <Card.Title>{getHighlightedText(data?.name, query)}</Card.Title>
                </Card.Header>
                <Card.Body>
                  <Card.Text key={data.id + "brewery_type"}>
                      {"Brewery Type"}: {(getHighlightedText((data as any)["brewery_type"], query))}
                  </Card.Text>
                  <Card.Text key={data.id + "city"}>
                      {"City"}: {(getHighlightedText((data as any)["city"], query))}
                  </Card.Text>
                  <Card.Text key={data.id + "longitude"}>
                      {"Longitude"}: {(getHighlightedText((data as any)["longitude"], query))}
                  </Card.Text>
                  <Card.Text key={data.id + "latitude"}>
                      {"Latitude"}: {(getHighlightedText((data as any)["latitude"], query))}
                  </Card.Text>
                  <Card.Text key={data.id + "website"}>
                      {"Website"}: {(getHighlightedText((data as any)["website"], query))}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          );
        })}
      </Row>
  )
}

export default function ExploreBreweries() {
  const history = useHistory();
  const search = useLocation().search;
  const page = parseInt(new URLSearchParams(search).get('page') ?? "1") 

  const [pageCount, setPageCount] = useState(1);
  const [breweriesCount, setBreweriesCount] = useState(0);
  const [currentBreweries, setCurrentBreweries] = useState<any[]>([]);
  const [sorting, setSorting] = useState({display: "name asc.", url: "name"})
  const [filter, setFilter] = useState<any>({})
  const [searchQuery, setSearchQuery] = useState("");

  async function getBreweriesData(pageNumber: number, sort: string, filter: string) {
    const requestParams = {
      "page[number]": pageNumber,
      "sort": sort,
      "filter[objects]": filter
    };
    const { data } = await makeAPIRequest("brewery", requestParams);
    setCurrentBreweries(data.data);
    setBreweriesCount(data.meta.total)
    setPageCount(Math.floor(data.meta.total / 12))
    //console.log('got new data', data)
  }

  useEffect(() => {

    //console.log("running use effect", pageNumber, sorting.url, filter)
    var filter_string = ""
    if (Object.keys(filter).length === 1) {
      filter_string = JSON.stringify(Object.values(filter))
    } else if (Object.keys(filter).length > 1) {
      filter_string = JSON.stringify([{ "and": Object.values(filter) }])
    }
    //console.log(filter_string)
    getBreweriesData(page, sorting.url, filter_string);
  }, [page, sorting, filter, currentBreweries]);


  function handleCardClick(id: number) {
    history.push(`/breweries/${id}`);
  }

  const goToPage = (number: number) => {
    history.push(`/breweries?page=${number}`)
  }

  function handleQueryChange(event: any) {
    setSearchQuery(event.target.value)
  }

  return (
    <Container fluid className="mt-4">
      <h2 className="text-center mt-3">Explore Breweries</h2>
      <h4>Found {breweriesCount || 0} matching results</h4>
      <Row>
        <Col style={{display: 'flex', justifyContent: 'center'}}>
          <DropdownButton id="sort-dropdown" title={`Sort By: ${sorting.display}`}>
            {BREWERY_SORTING_FUNCS.map((sort, index) => (
              <Dropdown.Item key={index} onClick={() => setSorting(sort)}>{sort.display}</Dropdown.Item>
            ))}
          </DropdownButton>
        </Col>
        <Col style={{display: 'flex', justifyContent: 'center'}}>
          <input type="text" value={searchQuery || ''} onChange={handleQueryChange} />
        </Col>

        <Col style={{display: 'flex', justifyContent: 'center'}}>
          <DropdownButton as={ButtonGroup} title="type">
            {BREWERY_TYPES.map(option => (
              <Dropdown.Item active={"brewery_type" in filter && filter['brewery_type'].val === option} onClick={() => setFilter({...filter, brewery_type: {"name":"brewery_type","op":"==","val":option}})}>{option}</Dropdown.Item>
            ))}
          </DropdownButton>

          <DropdownButton as={ButtonGroup} title="website">
            <Dropdown.Item active={"website" in filter && filter['website'].op === "is_not_null"} onClick={() => setFilter({...filter, website: {"name":"website","op":"is_not_null"}})}>has website</Dropdown.Item>
            <Dropdown.Item active={"website" in filter && filter['website'].op === "is_null"} onClick={() => setFilter({...filter, website: {"name":"website","op":"is_null"}})}>no website</Dropdown.Item>

          </DropdownButton>

          <DropdownButton as={ButtonGroup} title="phone number">
            <Dropdown.Item active={"phone" in filter && filter['phone'].op === "is_not_null"} onClick={() => setFilter({...filter, phone: {"name":"phone","op":"is_not_null"}})}>has phone</Dropdown.Item>
            <Dropdown.Item active={"phone" in filter && filter['phone'].op === "is_null"} onClick={() => setFilter({...filter, phone: {"name":"phone","op":"is_null"}})}>no phone</Dropdown.Item>
          </DropdownButton>
        </Col>
      </Row>

      <BreweriesGrid query={searchQuery} breweries={currentBreweries} handleCardClick={handleCardClick} />
      <br/>
      <Pagination size='lg' style={{display: 'flex', justifyContent: 'center'}}>
        <Pagination.First onClick={() => goToPage(1)} />
        <Pagination.Prev onClick={() => goToPage(Math.max(page - 1, 1))} />
        <Pagination.Item onClick={() => goToPage(page)} >{page}</Pagination.Item>
        <Pagination.Next onClick={() => goToPage(Math.min(page + 1, pageCount + 1))}/>
        <Pagination.Last onClick={() => goToPage(pageCount + 1)} />
      </Pagination>
    </Container>
  )
}

