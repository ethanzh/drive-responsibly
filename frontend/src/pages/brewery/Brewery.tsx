import React, { useEffect, useState } from "react";
import { Card, Container } from "react-bootstrap";
import { RouteComponentProps } from "react-router";
import { transpileModule } from "typescript";
import GoogleMap from "../../components/GoogleMap";
import TwitterCard from "../../components/TwitterCard";
import TwitterFeed from "../../components/TwitterFeed";
import { CountyConnection } from "../../components/CountyConnection";
import { makeAPIRequest, getTweets, getMap } from "../../utilities/api.js";

interface MatchParams {
  id: string;
}

const BREWERY_KEYS_TO_DISPLAY = [
  {label: "id", value: "ID"},
  {label: "brewery_type", value: "Brewery Type"},
  {label: "street", value: "Street"},
  {label: "postal_code", value: "Postal Code"},
  {label: "city", value: "City"},
  {label: "created_at", value: "Created At"},
  {label: "latitude", value: "Latitude"},
  {label: "longitude", value: "Longitude"},
  {label: "phone", value: "Phone"},
  {label: "website_url", value: "Website URL"},
];

const NearbyIncidents = ({ brewery_id }: { brewery_id: number })=> {
  const [incidents, setIncidents] = useState<any[]>()
  useEffect(() => {
    async function getbreweryData() {
      var { data } = await makeAPIRequest(`nearby_incidents/${brewery_id}`, {});
      setIncidents(data);
      console.log('found incidents', data);
    }
    getbreweryData();
  }, [])

  return incidents ? (
    <>
      <h4>Nearby Incidents</h4>
      <ul>
        {incidents.map((incident: any) => (
          <li key={incident.id}>
            <a href={`#/incidents/${incident.incident_id}`}>Incident #{incident.id}</a>
          </li>
        ))}
      </ul>
    </>
  ): (
    <p>Looking for Incidents nearby</p>
  )
}

export default function Brewery(props: RouteComponentProps<MatchParams>) {
  const [loading, setLoading] = useState(false);
  const [breweryData, setBreweryData] = useState<any>();
  const [tweets, setTweets] = useState<any>([]);
  const [map, setMap] = useState<any>();

  useEffect(() => {
    async function getBreweryData() {
      setLoading(true);
      var { data } = await makeAPIRequest(
        `brewery/${props.match.params.id}`,
        {}
      );
      setBreweryData(data.data);
      // console.log(data);
      data = data.data;
      const embedTweets = await getTweets([
        {
          query: data.attributes.name,
        },
        { query: `${data.attributes.city} brewery` },
        { query: `${data.attributes.city} bar` },
        { query: `${data.attributes.city} beer` },
        { query: `${data.attributes.city}` },
      ]);
      console.log("tweets", tweets);
      setTweets(embedTweets);
      const mapRes = await getMap(
        data.attributes.latitude,
        data.attributes.longitude,
        15
      );
      setMap(mapRes);
      setLoading(false);
    }
    getBreweryData();
  }, [props.match.params.id]);

  return !breweryData ? (
    <h1>loading</h1>
  ) : (
    <Container>
      <Card className="mt-4">
        <Card.Header>{breweryData.attributes.name}</Card.Header>
        <Card.Body>
          {/* <Card.Img variant="top" src={data?.image}></Card.Img> */}
          { breweryData &&
            <Card.Text key={breweryData.id + "id"}>
                {"ID"}: {(breweryData.attributes as any)["id"] || "N/A"}
            </Card.Text>
          }
          { breweryData &&
            <Card.Text key={breweryData.id + "brewery_type"}>
                {"Brewery Type"}: {(breweryData.attributes as any)["brewery_type"] || "N/A"}
            </Card.Text>
          }
          { breweryData &&
            <Card.Text key={breweryData.id + "address"}>
                {"Street"}: {(breweryData.attributes as any)["address"] || "N/A"}
            </Card.Text>
          }
          { breweryData &&
            <Card.Text key={breweryData.id + "postal_code"}>
                {"Postal Code"}: {(breweryData.attributes as any)["postal_code"] || "N/A"}
            </Card.Text>
          }
          { breweryData &&
            <Card.Text key={breweryData.id + "city"}>
                {"City"}: {(breweryData.attributes as any)["city"] || "N/A"}
            </Card.Text>
          }
          { breweryData &&
            <Card.Text key={breweryData.id + "created_at"}>
                {"Created At"}: {(breweryData.attributes as any)["created_at"] || "N/A"}
            </Card.Text>
          }
          { breweryData &&
            <Card.Text key={breweryData.id + "latitude"}>
                {"Latitude"}: {(breweryData.attributes as any)["latitude"] || "N/A"}
            </Card.Text>
          }
          { breweryData &&
            <Card.Text key={breweryData.id + "longitude"}>
                {"Longitude"}: {(breweryData.attributes as any)["longitude"] || "N/A"}
            </Card.Text>
          }
          { breweryData &&
            <Card.Text key={breweryData.id + "phone"}>
                {"Phone"}: {String((breweryData.attributes as any)["phone"]).slice(0,10) || "N/A"}
            </Card.Text>
          }
          { breweryData &&
            <Card.Text key={breweryData.id + "website"}>
                {"Website URL"}: {(breweryData.attributes as any)["website"] || "N/A"}
            </Card.Text>
          }
          <CountyConnection county_id={breweryData.relationships.county.data.id} />
          <NearbyIncidents brewery_id={parseInt(props.match.params.id)}/>
        </Card.Body>
      </Card>
      
      <GoogleMap src={map} />
      <TwitterFeed tweets={tweets} />
    </Container>
  );
}

