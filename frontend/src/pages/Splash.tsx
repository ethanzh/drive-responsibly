import React from "react";
import { Link } from "react-router-dom";
import { Card, Button, Container, Row, Col } from "react-bootstrap";
import brewery from './brewery.jpg'
import county from './county.gif'
import incident from './incident.jpg'

interface PageCardInterface {
  url: string;
  title: string;
  img: string;
}

const PageCard = ({ url, title, img }: PageCardInterface) => (
  <Card style={{ width: "18rem" }}>
    <Card.Img variant="top" src={img} />
    <Card.Body>
      <Button variant="primary" href={url}>
        {title}
      </Button>
    </Card.Body>
  </Card>
);

export default function Splash() {
  return (
    <div>
      <Container>
        <Row>
          <Col className="text-center">
            <h1>Drive Responsibly</h1>
          </Col>
        </Row>
        <Row>
          <Col className="text-center">
            <h4>
              Do breweries impact the safety of roads in their surrounding
              areas?
            </h4>
          </Col>
        </Row>
        <Row>
          <Col>
            <PageCard
              url="#/breweries"
              title="Explore Breweries"
              img={brewery}
            />
          </Col>
          <Col>
            <PageCard
              url="#/counties"
              title="Explore Counties"
              img={county}
            />
          </Col>
          <Col>
            <PageCard
              url="#/incidents"
              title="Explore Traffic Incidents"
              img={incident}
            />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
