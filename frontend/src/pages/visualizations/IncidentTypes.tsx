import React, { useEffect, useState } from "react";
import { makeAPIRequest } from "../../utilities/api.js";
import {
  CartesianGrid,
  Cell,
  Legend,
  Line,
  LineChart,
  Pie,
  PieChart,
  XAxis,
  YAxis,
} from "recharts";
import { Card, Tooltip } from "react-bootstrap";

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];

export default function IncidentTypesVisualization() {
  const [incidentData, setIncidentData] = useState<any[]>([]);
  useEffect(() => {
    async function getData() {
      var dataArr: any = [];
      var pageNum = 1;
      var { data } = await makeAPIRequest(`traffic_incident`, {
        "page[size]": 100,
        "page[number]": pageNum,
      });
      // dataArr.push(...data.data);

      while (data.data.length > 0) {
        dataArr.push(...data.data);
        pageNum += 1;
        var { data } = await makeAPIRequest(`traffic_incident`, {
          "page[size]": 100,
          "page[number]": pageNum,
        });
      }
      let accidentsByYear: any = {};
      let fatalitiesByYear: any = {};
      let allYrs: any = new Set([]);
      dataArr.forEach((incident: any) => {
        let year = incident.attributes.time_period.split("-")[0];
        if (!(year in accidentsByYear)) accidentsByYear[year] = 0;
        if (!(year in fatalitiesByYear)) fatalitiesByYear[year] = 0;
        // overallData[time] += 1;
        accidentsByYear[year] += 1;
        fatalitiesByYear[year] += incident.attributes.fatalities;
        allYrs.add(year);
      });
      console.log(allYrs);
      let formattedData: any = [];
      Object.keys(accidentsByYear).forEach((year) => {
        formattedData.push({
          year,
          numAccidents: accidentsByYear[year],
          numFatalities: fatalitiesByYear[year],
        });
      });

      setIncidentData(formattedData);
      console.log("found incidents", data);
      console.log("formatted data", formattedData);
    }
    getData();
  }, []);

  let renderLabel = function (entry: any) {
    console.log(entry);
    return `${entry.name} (${entry.value})`;
  };

  let getFill = function (entry: any) {
    console.log(entry);
    return `${entry.name} (${entry.value})`;
  };

  return (
    //   <ResponsiveContainer width="100%" height="100%">
    <Card className="h-100">
      <Card.Header>
        <Card.Title className="text-center">
          Incidents and Fatalities over time
        </Card.Title>
      </Card.Header>
      <Card.Body>
        <LineChart
          width={1000}
          height={800}
          data={incidentData}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis startOffset={2014} dataKey="year" />
          <YAxis domain={[0, 5000]} />
          {/* <Tooltip /> */}
          <Legend />
          <Line
            type="monotone"
            dataKey="numAccidents"
            stroke="#8884d8"
            activeDot={{ r: 8 }}
          />
          <Line type="monotone" dataKey="numFatalities" stroke="#82ca9d" />
        </LineChart>
      </Card.Body>
    </Card>
  );
}
