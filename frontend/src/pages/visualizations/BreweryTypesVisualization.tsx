import React, { useEffect, useState } from "react";
import { makeAPIRequest } from "../../utilities/api.js";
import { Cell, Pie, PieChart } from "recharts";
import { Card } from "react-bootstrap";

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];

export default function BreweryTypesVisualization() {
  const [breweryTypes, setBreweryTypes] = useState<any[]>([]);
  useEffect(() => {
    async function getData() {
      var dataArr: any = [];
      var pageNum = 1;
      var { data } = await makeAPIRequest(`brewery`, {
        "page[size]": 100,
        "page[number]": pageNum,
      });
      while (data.data.length > 0) {
        dataArr.push(...data.data);
        pageNum += 1;
        var { data } = await makeAPIRequest(`brewery`, {
          "page[size]": 100,
          "page[number]": pageNum,
        });
      }
      let overallData: any = {};
      dataArr.forEach((county: any) => {
        let breweryType = county.attributes.brewery_type;
        if (!(breweryType in overallData)) overallData[breweryType] = 0;
        overallData[breweryType] += 1;
      });
      let formattedData: any = [];
      Object.entries(overallData).forEach(([name, value]) => {
        formattedData.push({
          name,
          value,
        });
      });

      setBreweryTypes(formattedData);
      console.log("found breweries", data);
      console.log("formatted data", formattedData);
    }
    getData();
  }, []);

  let renderLabel = function (entry: any) {
    console.log(entry);
    return `${entry.name} (${entry.value})`;
  };

  let getFill = function (entry: any) {
    console.log(entry);
    return `${entry.name} (${entry.value})`;
  };

  return (
    //   <ResponsiveContainer width="100%" height="100%">
    <Card className="h-100">
      <Card.Header>
        <Card.Title className="text-center">
          Distribution of Brewery Types
        </Card.Title>
      </Card.Header>
      <Card.Body>
        <PieChart width={800} height={600}>
          <Pie
            data={breweryTypes}
            dataKey="value"
            nameKey="name"
            label={renderLabel}
            labelLine={false}
          >
            {breweryTypes.map((entry, index) => {
              return (
                <Cell
                  key={`cell-${index}`}
                  fill={COLORS[index % COLORS.length]}
                />
              );
            })}
          </Pie>
        </PieChart>
      </Card.Body>
    </Card>
  );
}
