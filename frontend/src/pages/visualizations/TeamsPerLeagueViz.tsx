import React, { useEffect, useState } from "react";
import { Card, Row } from "react-bootstrap";
import axios from "axios";
import {
    CartesianGrid, BarChart, XAxis, YAxis, Tooltip, Legend, Bar
} from "recharts";

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];

export default function TeamsPerLeagueViz() {
    const [teams, setTeams] = React.useState<any[]>([]);


    async function getData() {
        var pageNum = 1
        let data: any[] = [];
        while(pageNum < 3) {
            const result = await axios.get("https://api.welikesportz.me/teams?page=" + pageNum)
            data.push(...result.data.objects);
            ++pageNum
        }
        return data;
    }

    useEffect(() => {
        getData().then(data => {
            let rawTeamData = data.map(team => ({
                "name": team.teams_name,
                "sport": team.teams_sport,
            }));
            let sportCount: any = {};
            rawTeamData.forEach((team: any) => {
                let sport = team.sport;
                if (!(sport in sportCount)) sportCount[sport] = 0;
                sportCount[sport] += 1;
            });
            let formattedData: any = [];
            Object.entries(sportCount).forEach(([sport, num_teams]) => {
                formattedData.push({
                    sport,
                    num_teams,
                });
            });
            setTeams(formattedData);
        });
    }, []);

  return (
    <Card className="h-100">
      <Card.Header>
        <Card.Title className="text-center">
          Number of teams for each sport
        </Card.Title>
      </Card.Header>
      <Card.Body>
      <Row className="justify-content-center">
        <BarChart width={1200} height={600} data={teams}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="sport" />
            <YAxis dataKey="num_teams"/>
            <Tooltip />
            <Legend />
            <Bar dataKey="num_teams" fill="#8884d8" />
        </BarChart>
      </Row>
      </Card.Body>
    </Card>
  );
}
