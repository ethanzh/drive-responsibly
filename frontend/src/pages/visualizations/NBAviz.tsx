import React, { useRef, useEffect } from "react";
import {
    BarChart, CartesianGrid, Legend, XAxis, YAxis, Tooltip, Bar,
    ScatterChart, Scatter, Label, PieChart, Pie, ResponsiveContainer, Cell
} from 'recharts';
import axios from "axios"

const PlayerTooltip = ({ active, payload, label }: { active?: boolean, payload?: any[], label?: string }) => {
    console.log(payload, label)

    if (active && payload && payload.length) {
        let player = payload[0].payload
        return (
            <div className="custom-tooltip">
                <p className="label">{player.name}</p>
                <p className="label">weight: {player.weight}</p>
                <p className="label">height: {player.height}</p>
                <p className="label">city: {player.city}</p>
                <p className="label">team: {player.team}</p>
            </div>
        );
    }

    return null;
};


function findHeight(height: any) {
    if (height === null) {
        return -1;
    }
    const values = height.match(/\d+/g);
    if (values === null || values[0] > 7 || values[0] < 4) {
        return -1;
    }
    const feet = parseFloat(values[0]);
    const inches = values[1] !== null ? parseFloat(values[1]) : 0;
    return feet + inches / 12;
}

function findWeight(weight: any) {
    if (weight === null) {
        return -1;
    }
    const values = weight.match(/\d+/g);
    if (values === null) {
        return -1;
    }
    const other = values[1] !== null ? parseInt(values[1]) : 0;
    return Math.max(other, parseInt(values[0]));
}




export default function NBAViz() {
    const [players, setPlayers] = React.useState<any[]>([]);

    useEffect(() => {
        async function getPlayers() {
            axios.get(`https://api.welikesportz.me/players?q={"order_by":[{"field":"players_name","direction":"asc"}],"filters":[{"name":"players_sport","op":"eq","val":"Basketball"}]}`)
                .then(resp => {
                    const data = resp.data.objects.map((player: any) => ({
                        "name": player.players_name,
                        "height": findHeight(player.players_height),
                        "weight": findWeight(player.players_weight),
                        "team": player.players_team,
                        "city": player.players_city
                    }))
                    console.log(data)
                    setPlayers(data)
                })
        }

        getPlayers()
    }, [])


    return (
        <div style={{ width: "100%", height: "500px" }}>
            <h3 className="text-center">Players</h3>

            <ResponsiveContainer width="100%" height="100%">
                <ScatterChart
                    width={400}
                    height={400}
                    margin={{
                        top: 20,
                        right: 20,
                        bottom: 20,
                        left: 20,
                    }}
                >
                    <CartesianGrid />
                    <XAxis
                        type="number" dataKey="weight" name="weight" unit="lb"
                        label={{ value: "Weight", position: 'bottom' }}
                        allowDataOverflow
                        domain={[150, "dataMax + 10"]}
                        tickCount={8}
                    />
                    <YAxis
                        type="number" dataKey="height" name="height" unit="ft"
                        label={{ value: "Height", position: 'left', angle: -90, offset: 10 }}
                        domain={[5, "dataMax + 1"]}
                        allowDataOverflow
                        tickCount={8}
                    />
                    <Tooltip
                        cursor={{ stroke: 'red', strokeWidth: 2 }}
                        content={< PlayerTooltip />}
                    />
                    <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                    <Scatter name="Players" data={players} fill="#8884d8" />
                </ScatterChart>
            </ResponsiveContainer>


        </div>
    )
}