import TeamsPerLeagueViz from "./TeamsPerLeagueViz";
import HeightViz from "./HeightViz";
import NBAViz from "./NBAviz";
import { Row } from "react-bootstrap";

export default function WLSVisualizations() {
  return (
    <>
      <Row className="justify-content-center">
      <h1>WeLikeSportz Visualizations</h1>
      </Row>
      <TeamsPerLeagueViz />
      <HeightViz />
      <NBAViz />
    </>
  );
}
