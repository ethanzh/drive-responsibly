import BreweryTypesVisualization from "./BreweryTypesVisualization";
import CountyViz from "./CountyViz";
import IncidentTypesVisualization from "./IncidentTypes";

export default function Visualizations() {
  return (
    <>
      <h1>Visualizations</h1>
      <CountyViz />
      <BreweryTypesVisualization />
      <IncidentTypesVisualization />
    </>
  );
}
