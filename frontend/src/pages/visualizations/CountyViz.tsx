import React, { useRef, useEffect } from "react";
import {
  BarChart,
  CartesianGrid,
  Legend,
  XAxis,
  YAxis,
  Tooltip,
  Bar,
  ScatterChart,
  Scatter,
  Label,
  PieChart,
  Pie,
  ResponsiveContainer,
  Cell,
} from "recharts";
import { makeAPIRequest } from "../../utilities/api.js";

function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const CountyTooltip = ({
  active,
  payload,
  label,
}: {
  active?: boolean;
  payload?: any[];
  label?: string;
}) => {
  if (active && payload && payload.length) {
    return (
      <div className="custom-tooltip">
        <p className="label">{payload[0].payload.name}</p>
        <p className="label">{`${payload[0].name} : ${numberWithCommas(
          payload[0].value
        )}`}</p>
        <p className="label">{`${payload[1].name} : ${numberWithCommas(
          payload[1].value
        )}`}</p>
      </div>
    );
  }

  return null;
};

export default function CountyViz() {
  const [counties, setCounties] = React.useState<any[]>([]);

  async function getCounties() {
    return Promise.all([
      makeAPIRequest("county", {
        "page[number]": 1,
        sort: "name",
        "page[size]": 100, // thats the max page size looks like
      }),
      makeAPIRequest("county", {
        "page[number]": 2,
        sort: "name",
        "page[size]": 100,
      }),
      makeAPIRequest("county", {
        "page[number]": 3,
        sort: "name",
        "page[size]": 100,
      }),
    ]).then((responses) => {
      let data: any[] = [];
      responses.forEach((res) => {
        data.push(...res.data.data);
      });
      return data;
    });
  }

  useEffect(() => {
    getCounties().then((data) => {
      let countyData = data.map((county) => ({
        name: county.attributes.name,
        population: county.attributes.population,
        income: county.attributes.median_household_income,
      }));
      setCounties(countyData);
    });
  }, []);

  return (
    <div style={{ width: "100%" }}>
      <h3 className="text-center">Counties</h3>
      {/* <ResponsiveContainer width="100%" height="100%"> */}
      <ScatterChart
        width={1200}
        height={800}
        margin={{
          top: 20,
          right: 20,
          bottom: 20,
          left: 20,
        }}
      >
        <CartesianGrid />
        <XAxis
          type="number"
          dataKey="population"
          name="population"
          scale="sqrt"
          label={{ value: "Population", position: "bottom" }}
          ticks={[1000, 40000, 500000, 1500000, 2500000, 5000000]}
          tickFormatter={numberWithCommas}
        />
        <YAxis
          type="number"
          dataKey="income"
          name="income"
          label={{
            value: "Median Household Income",
            position: "left",
            angle: -90,
            offset: 10,
          }}
          ticks={[30000, 50000, 70000, 90000, 110000]}
          tickFormatter={numberWithCommas}
        />
        <Tooltip
          cursor={{ stroke: "red", strokeWidth: 2 }}
          content={<CountyTooltip />}
        />
        <Tooltip cursor={{ strokeDasharray: "3 3" }} />
        <Scatter name="Counties" data={counties} fill="#8884d8" />
      </ScatterChart>
      {/* </ResponsiveContainer> */}
    </div>
  );
}
