import React, { useEffect, useState } from "react";
import { Card, Row } from "react-bootstrap";
import axios from "axios";
import {
    LineChart, XAxis, YAxis, CartesianGrid, Tooltip, Line
} from "recharts";

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];

export default function HeightViz() {
    const [players, setPlayers] = React.useState<any[]>([]);


    async function getData() {
      var pageNum = 1;
      let data: any[] = [];
      while(pageNum < 20) {
        var result = await axios.get("https://api.welikesportz.me/players?page=" + pageNum);
        data.push(...result.data.objects);
        pageNum += 1;
      }
      return data;
    }

    useEffect(() => {
        getData().then(data => {
            let rawTeamData = data.map(players => ({
                "height": players.players_height,
                "sport": players.players_sport,
            }));
            console.log("rawTeamData", rawTeamData)
            let basketballHeight: any = {};
            let footballHeight: any = {};
            let hockeyHeight: any = {};
            let soccerHeight: any = {};
            rawTeamData.forEach((player: any) => {
              let heightStr = player.height;
              let sport = player.sport;
              if(heightStr && heightStr.search(/\d\sft/) != -1 && heightStr.search(/\d\sin/) != -1) {
                let feet = parseInt(heightStr.substring(heightStr.search(/\d\sft/)));
                let inch = parseInt(heightStr.substring(heightStr.search(/\d\sin/)));
                let totalHeight = feet*12+inch;
                if(totalHeight != NaN) {
                  if(sport == "Basketball") {
                    if(!(totalHeight in basketballHeight))
                    basketballHeight[totalHeight] = 0;
                    basketballHeight[totalHeight] += 1;
                  }
                  else if (sport == "Soccer") {
                    if(!(totalHeight in soccerHeight))
                    soccerHeight[totalHeight] = 0;
                    soccerHeight[totalHeight] += 1;
                  }
                  else if (sport == "Ice Hockey") {
                    if(!(totalHeight in hockeyHeight))
                    hockeyHeight[totalHeight] = 0;
                    hockeyHeight[totalHeight] += 1;
                  }
                  else if (sport == "American Football") {
                    if(!(totalHeight in footballHeight))
                    footballHeight[totalHeight] = 0;
                    footballHeight[totalHeight] += 1;
                  }
                }
              }
            });
            let formattedData: any = [];
            var i = 55;
            while (i < 91)
            {
              let height = i;
              let basketball = 0;
              if(i in basketballHeight)
                basketball = basketballHeight[i];
              let soccer = 0;
              if(i in soccerHeight)
                soccer = soccerHeight[i];
              let football = 0;
              if(i in footballHeight)
                football = footballHeight[i];
              let icehockey = 0;
              if(i in hockeyHeight)
                icehockey = hockeyHeight[i];
              formattedData.push({
                  height,
                  basketball,
                  soccer,
                  football,
                  icehockey
                });
              i++;
            }
            console.log("fd", formattedData)
            setPlayers(formattedData);
        });
    }, []);

  return (
    <Card className="h-100">
      <Card.Header>
        <Card.Title className="text-center">
          Players' height by sport
        </Card.Title>
      </Card.Header>
      <Card.Body>
      <Row className="justify-content-center">
      <LineChart width={1200} height={600} data={players}
        margin={{ top: 10, right: 30, left: 20, bottom: 30 }}>
        <XAxis dataKey="height" label={{value: "Height in inches", position: "insideBottom", offset: -15}}/>
        <YAxis label={{value: "Number of players", angle: -90, position: "insideLeft", offset: 10}}/>
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip />
        <Line type="monotone" dataKey="basketball" stroke="#992222" />
        <Line type="monotone" dataKey="soccer" stroke="#229922" />
        <Line type="monotone" dataKey="icehockey" stroke="#222299" />
        <Line type="monotone" dataKey="football" stroke="#444444" />
      </LineChart>
      </Row>
      </Card.Body>
    </Card>
  );
}
