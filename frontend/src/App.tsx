import "./App.css";
import Navigation from "./components/Navigation";
import { HashRouter, Route, Switch } from "react-router-dom";
import Splash from "./pages/Splash";
import About from "./pages/About/About";
import Brewery from "./pages/brewery/Brewery";
import ExploreBreweries from "./pages/brewery/ExploreBreweries";
import ExploreCounties from "./pages/county/ExploreCounties";
import County from "./pages/county/County";
import ExploreIncidents from "./pages/incident/ExploreIncidents";
import Incident from "./pages/incident/Incident";
import Visualizations from "./pages/visualizations/Visualizations";
import WLSVisualizations from "./pages/visualizations/WLSVisualizations";

export function Routes() {
  return (
    <>
      <Switch>
        <Route exact path="/about" component={About} />
        <Route exact path="/" component={Splash} />
        <Route exact path="/breweries" component={ExploreBreweries}></Route>
        <Route path="/breweries/:id" component={Brewery}></Route>
        <Route exact path="/counties" component={ExploreCounties}></Route>
        <Route path="/counties/:id" component={County}></Route>
        <Route exact path="/incidents" component={ExploreIncidents}></Route>
        <Route path="/incidents/:id" component={Incident}></Route>
        <Route path="/visualizations" component={Visualizations}></Route>
        <Route path="/wlsvisualizations" component={WLSVisualizations}></Route>
      </Switch>
    </>
  );
}

export function App() {
  return (
    <>
      <HashRouter>
        <Navigation />
        <Routes />
      </HashRouter>
    </>
  );
}

export default App;
