import { Card, Row, Col } from "react-bootstrap";
import { RouteComponentProps } from "react-router-dom";
import twitterIcon from "../twitter-transparent.png";

interface GoogleMapProps {
  src: string;
}

export default function GoogleMap(props: GoogleMapProps) {
  return (
    <Card className="mt-4">
      <Card.Header>Location Map</Card.Header>
      <Card.Body className="text-center">
        <img src={props.src} className="w-100" />
      </Card.Body>
    </Card>
  );
}
