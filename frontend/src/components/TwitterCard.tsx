import { Card, Row, Col } from "react-bootstrap";
import { RouteComponentProps } from "react-router-dom";
import twitterIcon from "../twitter-transparent.png";

interface TwitterCardProps {
  tweetHtml: string;
  authorName: string;
}

export default function TwitterCard(props: TwitterCardProps) {
  return (
    <Card className="mt-4">
      <Card.Header>
        <Row>
          <Col>Tweet from User: {props.authorName}</Col>

          <Col xs={2}>
            <img
              src={twitterIcon}
              style={{ maxHeight: "25px" }}
              className="h-100 float-right"
            ></img>
          </Col>
        </Row>
      </Card.Header>
      <Card.Body
        dangerouslySetInnerHTML={{
          __html: props.tweetHtml,
        }}
      ></Card.Body>
    </Card>
  );
}
