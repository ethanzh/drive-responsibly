import { Navbar, Nav, Container } from "react-bootstrap";

export default function Navigation() {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="#/">Drive Responsibly</Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link href="/#/">Home</Nav.Link>
          <Nav.Link href="/#/about">About</Nav.Link>
          <Nav.Link href="/#/breweries">Breweries</Nav.Link>
          <Nav.Link href="/#/counties">Counties</Nav.Link>
          <Nav.Link href="/#/incidents">Incidents</Nav.Link>
          <Nav.Link href="/#/visualizations">Visualizations</Nav.Link>
          <Nav.Link href="/#/wlsvisualizations">Their Visualizations</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
}
