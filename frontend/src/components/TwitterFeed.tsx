import { Card, Row, Col } from "react-bootstrap";
import TwitterCard from "./TwitterCard";
interface TwitterFeedProps {
  tweets: any;
}

export default function TwitterFeed(props: TwitterFeedProps) {
  return (
    <Card className="mt-4">
      <Card.Header>Recent Related Tweets</Card.Header>
      <Card.Body>
        {props.tweets.map((tweet: any) => {
          return (
            <TwitterCard
              key={`${tweet.data.html}`}
              authorName={tweet.data.author_name}
              tweetHtml={tweet.data.html}
            />
          );
        })}
      </Card.Body>
    </Card>
  );
}
