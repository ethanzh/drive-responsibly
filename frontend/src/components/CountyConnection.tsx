import React, {useState, useEffect} from "react"
import { makeAPIRequest} from "../utilities/api.js";

export const CountyConnection = ({ county_id }: {county_id: number}) => {
    const [countyData, setCountyData] = useState<any>()
    useEffect(() => {
      async function getCountyData() {
        var { data } = await makeAPIRequest(`county/${county_id}`, {});
        setCountyData(data.data);
        console.log('got county data', data);
      }
      getCountyData();
    }, [])
    
    return countyData ? (
      <>
      County: <a href={`#/counties/${county_id}`}>{countyData?.attributes?.name}</a>
      </>
    ) : (
      <p>County: searching...</p>
    )
  }