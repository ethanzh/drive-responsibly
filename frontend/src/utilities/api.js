import axios from "axios";

// const TWITTER_TOKEN = process.env.REACT_APP_TWITTER_API_TOKEN
const TWITTER_TOKEN = "AAAAAAAAAAAAAAAAAAAAAJyTOgEAAAAAh3Ikb%2BQb7tUy%2BNHjMO7d57Rt8Qg%3DLZyqiOW6NFp3hIPr7XYIk154c0DL4jZyuVZjSj6it5k1KFSyBy"
const GOOGLE_MAPS_TOKEN = "AIzaSyB3ud_Q0xFZpKpH75uE2kCXe0a7z1VeWJc"
console.log("token", TWITTER_TOKEN)
const axiosClient = axios.create({
  baseURL: "https://api.driveresponsibly.me/api"
  // baseURL: "http://10.17.3.35:5000/api"
});

const compileRequestParams = (params) => {
  let requestParams = new URLSearchParams();
  for (const paramName in params) {
    if (Array.isArray(params[paramName])) {
      for (const paramValue of params[paramName]) {
        if (paramValue !== "") {
          requestParams.append(paramName, paramValue);
        }
      }
    } else {
      if (params[paramName] !== "") {
        requestParams.append(paramName, params[paramName]);
      }
    }
  }
  return requestParams;
};

export const makeAPIRequest = async (endpoint, params) => {
  const result = await axiosClient.get(endpoint, {
    params: compileRequestParams(params),
    headers: {
      "Content-Type": "application/vnd.api+json",
      "Accept": "application/vnd.api+json"
    }
  });
  return result;
};

export const getTweets = async (paramsList) => { // should just pass in one param with key "query"
  const PROXY_URL = "https://whispering-spire-78473.herokuapp.com/";
  const TWITTER_SEARCH_ENDPOINT = PROXY_URL + "https://api.twitter.com/2/tweets/search/recent"
  const TWITTER_EMBED_ENDPOINT = PROXY_URL + "https://publish.twitter.com/oembed"
  console.log("params")
  var finalRes
  for (var i = 0; i < paramsList.length; i++) {
    var params = paramsList[i]
    params = {
      ...params,
      "expansions": "attachments.media_keys",
      "media.fields": "duration_ms,height,media_key,preview_image_url,type,url,width,public_metrics,non_public_metrics,organic_metrics,promoted_metrics,alt_text"
    }
    const res = await axios.get(`${TWITTER_SEARCH_ENDPOINT}`, {
      params: compileRequestParams(params),
      headers: {
        "Authorization": `Bearer ${TWITTER_TOKEN}`
      }
    }).catch(function (error) {
      if (error.response) {
        // Request made and server responded
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
      }

    })
    console.log("res", params, res.data.data)
    if (res.data && res.data.meta.result_count > 0) {
      console.log()
      finalRes = res;
      break
    }
  }
  if (finalRes == null) return []
  console.log("res final", finalRes.data.data)
  const tweets = finalRes.data.data
  var embedResults = []
  for (var i = 0; i < tweets.length; i++) {
    const id = tweets[i].id
    const embed_res = await axios.get(`${TWITTER_EMBED_ENDPOINT}`, {
      params: compileRequestParams({ url: `https://twitter.com/twitter/status/${id}` }),
      headers: {
        "Authorization": `Bearer ${TWITTER_TOKEN}`
      }
    })
    console.log("embed", embed_res.data.html)
    embedResults.push(embed_res)
  }
  console.log("embed results", embedResults)
  return embedResults
}

export const getMap = async (latitude, longitude, zoom) => {
  const GOOGLE_MAPS_ENDPOINT = "https://maps.googleapis.com/maps/api/staticmap"
  // const params = {
  //   center: `${latitude},${longitude}`,
  //   zoom: 15,
  //   size: "500x400",
  //   key: "AIzaSyBHqM3RsLkLLOhqwhsneWBBKim66ukJglQ"
  // }
  // console.log(GOOGLE_MAPS_ENDPOINT + "?" + compileRequestParams(params).toString())
  // const res = await axios.get("https://maps.googleapis.com/maps/api/staticmap?center=32.93044336,-96.89883128&zoom=15&size=500x400&key=AIzaSyBHqM3RsLkLLOhqwhsneWBBKim66ukJglQ").catch(function (error) {
  //   if (error.response) {
  //     // Request made and server responded
  //     console.log(error.response.data);
  //     console.log(error.response.status);
  //     console.log(error.response.headers);
  //   } else if (error.request) {
  //     // The request was made but no response was received
  //     console.log(error.request);
  //   } else {
  //     // Something happened in setting up the request that triggered an Error
  //     console.log('Error', error.message);
  //   }

  // })
  // console.log("map res", res)
  // return res.data
  // const res = `${GOOGLE_MAPS_ENDPOINT}?center=29.9515464,-95.5186591&zoom=${zoom}&size=700x700&key=${GOOGLE_MAPS_TOKEN}`
  const dynamicRes = `${GOOGLE_MAPS_ENDPOINT}?center=${latitude},${longitude}&zoom=${zoom}&size=700x600&key=${GOOGLE_MAPS_TOKEN}`
  // console.log("res", res)
  // console.log("res", dynamicRes)
  return dynamicRes
}