import unittest
from selenium import webdriver #connect to a browser instance
from selenium.webdriver.common.by import By #find elements by ...
from selenium.webdriver.chrome.options import Options #set command line switches https://peter.sh/experiments/chromium-command-line-switches/
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import sys

PATH = "./frontend/gui_tests/chromedriver_linux"
URL = "https://www.driveresponsibly.me/#/breweries"

class TestBrewery(unittest.TestCase):

    #set up browser and selenium
    def setUp(self):
        s = Service(PATH)
        testoptions = Options()
        testoptions.add_argument("--headless")
        testoptions.add_argument("--no-default-browser-check")
        testoptions.add_argument("--no-sandbox")
        testoptions.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(service=s, options=testoptions)
        self.driver.get(URL)

    #close browser and selenium
    def tearDown(self):
        self.driver.quit()
    
    #clicks navbar to home page
    def testBreweryToHome(self):
        self.driver.find_elements(By.CLASS_NAME, 'nav-link')[0].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h1')
        self.assertEqual(header.text, "Drive Responsibly")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)
    
    def testBrewerySort(self):
        #verify initially sorted by ascending name
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'card-header'))
            )
        except Exception as ex:
            print(ex)
            return
        ascCard = self.driver.find_element(By.CLASS_NAME, 'card-header')
        self.assertEqual(ascCard.text, "11 Below Brewing Company")

        #click dropdown to sort by descending name
        self.driver.find_element(By.ID, 'sort-dropdown').click()
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'dropdown-item'))
            )
        except Exception as ex:
            print(ex)
            return
        descButton = self.driver.find_elements(By.CLASS_NAME, 'dropdown-item')[1]
        self.assertEqual(descButton.text, "name desc.")
        descButton.click()

        #verify now sorted by descending name 
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.text_to_be_present_in_element((By.CLASS_NAME, 'card-header'), "Zilker Brewing Co")
            )
        except Exception as ex:
            print(ex)
            return
        descCard = self.driver.find_element(By.CLASS_NAME, 'card-header')
        self.assertEqual(descCard.text, "Zilker Brewing Co")

    def testBreweryFilter(self):
        #verify initially unfiltered
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'card-header'))
            )
        except Exception as ex:
            print("WebDriverWaitUntil Failed")
            self.assertEqual(False, True)
        ascCard = self.driver.find_element(By.CLASS_NAME, 'card-header')
        self.assertEqual(ascCard.text, "11 Below Brewing Company")

        #click dropdown to filter by large poverty
        self.driver.find_elements(By.CSS_SELECTOR, 'button.dropdown-toggle.btn.btn-primary')[1].click()
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'dropdown-item'))
            )
        except Exception as ex:
            print("WebDriverWaitUntil Failed")
            self.assertEqual(False, True)
        largeFilterButton = self.driver.find_elements(By.CLASS_NAME, 'dropdown-item')[5]
        self.assertEqual(largeFilterButton.text, "closed")
        largeFilterButton.click()

        #verify page shows new elements that were not filtered 
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.text_to_be_present_in_element((By.CLASS_NAME, 'card-header'), "Granary 'Cue and Brew")
            )
        except Exception as ex:
            print("WebDriverWaitUntil Failed")
            self.assertEqual(False, True)
        descCard = self.driver.find_element(By.CLASS_NAME, 'card-header')
        self.assertEqual(descCard.text, "Granary 'Cue and Brew")

    def testBrewerySearch(self):
        #verify initially unfiltered
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'card-header'))
            )
        except Exception as ex:
            print(ex)
            return
        initialCard = self.driver.find_element(By.CLASS_NAME, 'card-header')
        self.assertEqual(initialCard.text, "11 Below Brewing Company")
        
        #type "alamo" into search bar
        self.driver.find_elements(By.CSS_SELECTOR, 'input')[0].send_keys("Alamo")
        
        #verify first result is Alamo Beer Co
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.text_to_be_present_in_element((By.CLASS_NAME, 'card-header'), "Alamo Beer Co")
            )
        except Exception as ex:
            print("WebDriverWaitUntil Failed")
            self.assertEqual(False, True)
        searchedCard = self.driver.find_element(By.CLASS_NAME, 'card-header')
        self.assertEqual(searchedCard.text, "Alamo Beer Co")

if __name__ == '__main__':
    try:
        PATH = sys.argv[1]
    except IndexError:
        PATH = "./frontend/gui_tests/chromedriver.exe"
    unittest.main(argv=['first-arg-is-ignored'])
