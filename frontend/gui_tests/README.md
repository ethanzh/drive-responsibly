## Selenium Tests folder

Installation of Selenium libraries for Python can be done using pip:\
``pip install selenium``

Run all test files by running\
``python3 ./frontend/gui_tests/seleniumTests.py``\
from the repo's root directory
