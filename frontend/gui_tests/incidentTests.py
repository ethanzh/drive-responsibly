import unittest
from selenium import webdriver #connect to a browser instance
from selenium.webdriver.common.by import By #find elements by ...
from selenium.webdriver.chrome.options import Options #set command line switches https://peter.sh/experiments/chromium-command-line-switches/
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import sys

PATH = "./frontend/gui_tests/chromedriver_linux"
URL = "https://www.driveresponsibly.me/#/incidents"

class one_of_elements_has_text(object):
  """An expectation for checking that locator matches at least one element's text.

  locator - used to find the elements
  returns the WebElement once it has the particular css class
  """
  def __init__(self, locator, text):
    self.locator = locator
    self.text = text

  def __call__(self, driver):
    elements = driver.find_elements(*self.locator)   # Finding the referenced elements
    for element in elements :
        elementText = element.text
        if elementText == self.text :
            return element
    print("did not find element with text matching:")
    print(self.text)
    return False
    
class TestIncident(unittest.TestCase):

    #set up browser and selenium
    def setUp(self):
        s = Service(PATH)
        testoptions = Options()
        testoptions.add_argument("--headless")
        testoptions.add_argument("--no-default-browser-check")
        testoptions.add_argument("--no-sandbox")
        testoptions.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(service=s, options=testoptions)
        self.driver.get(URL)

    #close browser and selenium
    def tearDown(self):
        self.driver.quit()
    
    #clicks navbar to home page
    def testIncidentToHome(self):
        self.driver.find_elements(By.CLASS_NAME, 'nav-link')[0].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h1')
        self.assertEqual(header.text, "Drive Responsibly")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    def testIncidentSort(self):
        #verify initially sorted by ascending city name
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, 'div.rdg-cell.c1wupbe700-beta7'))
            )
        except Exception as ex:
            print("WebDriverWaitUntil Failed")
            self.assertEqual(False, True)
        incidentIDDiv = self.driver.find_elements(By.CSS_SELECTOR, 'div.rdg-cell.c1wupbe700-beta7')[7]
        self.assertEqual(incidentIDDiv.text, "667")

        #click dropdown to sort by descending name
        self.driver.find_element(By.ID, 'sort-dropdown').click()
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'dropdown-item'))
            )
        except Exception as ex:
            print("WebDriverWaitUntil Failed")
            self.assertEqual(False, True)
        descButton = self.driver.find_elements(By.CLASS_NAME, 'dropdown-item')[1]
        self.assertEqual(descButton.text, "city desc.")
        descButton.click()

        #verify now sorted by descending city name 
        try:
            element = WebDriverWait(self.driver, 1).until(
                one_of_elements_has_text((By.CSS_SELECTOR, 'div.rdg-cell.c1wupbe700-beta7'), "ZAPATA")
            )
        except Exception as ex:
            print("WebDriverWaitUntil Failed")
            self.assertEqual(False, True)
        incidentNameDiv = self.driver.find_elements(By.CSS_SELECTOR, 'div.rdg-cell.c1wupbe700-beta7')[8]
        self.assertEqual(incidentNameDiv.text, "ZAPATA")

    def testIncidentFilter(self):
        #verify initially unfiltered
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, 'div.rdg-cell.c1wupbe700-beta7'))
            )
        except Exception as ex:
            print("WebDriverWaitUntil Failed")
            self.assertEqual(False, True)
        incidentIDDiv = self.driver.find_elements(By.CSS_SELECTOR, 'div.rdg-cell.c1wupbe700-beta7')[7]
        self.assertEqual(incidentIDDiv.text, "667")

        #click dropdown to filter by dusk lighting
        self.driver.find_elements(By.CSS_SELECTOR, 'button.dropdown-toggle.btn.btn-primary')[2].click()
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'dropdown-item'))
            )
        except Exception as ex:
            print("WebDriverWaitUntil Failed")
            self.assertEqual(False, True)
        duskButton = self.driver.find_elements(By.CLASS_NAME, 'dropdown-item')[3]
        self.assertEqual(duskButton.text, "dusk")
        duskButton.click()

        #verify page shows new elements that were not filtered 
        try:
            element = WebDriverWait(self.driver, 1).until(
                one_of_elements_has_text((By.CSS_SELECTOR, 'div.rdg-cell.c1wupbe700-beta7'), "10920")
            )
        except Exception as ex:
            print("WebDriverWaitUntil Failed")
            self.assertEqual(False, True)
        incidentNameDiv = self.driver.find_elements(By.CSS_SELECTOR, 'div.rdg-cell.c1wupbe700-beta7')[7]
        self.assertEqual(incidentNameDiv.text, "8956")

if __name__ == '__main__':
    try:
        PATH = sys.argv[1]
    except IndexError:
        PATH = "./frontend/gui_tests/chromedriver.exe"
    unittest.main(argv=['first-arg-is-ignored'])