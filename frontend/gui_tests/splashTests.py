import unittest
from selenium import webdriver #connect to a browser instance
from selenium.webdriver.common.by import By #find elements by ...
from selenium.webdriver.chrome.options import Options #set command line switches https://peter.sh/experiments/chromium-command-line-switches/
from selenium.webdriver.chrome.service import Service
import sys

PATH = "./frontend/gui_tests/chromedriver_linux"
URL = "https://www.driveresponsibly.me/#/"

class TestSplash(unittest.TestCase):

    #set up browser and selenium
    def setUp(self):
        s = Service(PATH)
        testoptions = Options()
        testoptions.add_argument("--headless")
        testoptions.add_argument("--no-default-browser-check")
        testoptions.add_argument("--no-sandbox")
        testoptions.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(service=s, options=testoptions)
        self.driver.get(URL)

    #close browser and selenium
    def tearDown(self):
        self.driver.quit()
    
    #Checks website title, essentialy barebones test to ensure selenium working properly
    def testTitle(self):
        title = self.driver.title
        self.assertEqual(title, "Drive Responsibly")

    #clicks navbar "Drive Responsibly" text and expects to stay on splash page
    def testNavHomeText(self):
        self.driver.find_element(By.CLASS_NAME, 'navbar-brand').click() 
        header = self.driver.find_element(By.TAG_NAME, 'h1')
        self.assertEqual(header.text, "Drive Responsibly")

        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    #clicks navbar "Home" text and expects to stay on splash page
    def testNavHomeButton(self):
        self.driver.find_elements(By.CLASS_NAME, 'nav-link')[0].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h1')
        self.assertEqual(header.text, "Drive Responsibly")

        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    #clicks navbar "About" text and expects to go to About page
    def testNavAboutButton(self):
        self.driver.find_elements(By.CLASS_NAME, 'nav-link')[1].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h1')
        self.assertEqual(header.text, "About Us")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    #clicks navbar "Breweries" text and expects to go to Explore Breweries page
    def testNavBreweryButton(self):
        self.driver.find_elements(By.CLASS_NAME, 'nav-link')[2].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h2')
        self.assertEqual(header.text, "Explore Breweries")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    #clicks navbar "Breweries" text and expects to go to Explore Counties page
    def testNavCountyButton(self):
        self.driver.find_elements(By.CLASS_NAME, 'nav-link')[3].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h2')
        self.assertEqual(header.text, "Explore Counties")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    #clicks navbar "Incidents" text and expects to go to Explore Incidents page
    def testNavIncidentButton(self):
        self.driver.find_elements(By.CLASS_NAME, 'nav-link')[4].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h2')
        self.assertEqual(header.text, "Explore Incidents")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    #clicks Explore Breweries button and expects to go to Explore Breweries page
    def testExploreBreweriesButton(self):
        self.driver.find_elements(By.CLASS_NAME, 'btn')[0].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h2')
        self.assertEqual(header.text, "Explore Breweries")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    #clicks Explore Counties button and expects to go to Explore Counties page
    def testExploreCountiesButton(self):
        self.driver.find_elements(By.CLASS_NAME, 'btn')[1].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h2')
        self.assertEqual(header.text, "Explore Counties")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    #clicks Explore Traffic Incidents button and expects to go to Explore Incidents page
    def testExploreIncidentsButton(self):
        self.driver.find_elements(By.CLASS_NAME, 'btn')[2].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h2')
        self.assertEqual(header.text, "Explore Incidents")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

if __name__ == '__main__':
    try:
        PATH = sys.argv[1]
    except IndexError:
        PATH = "./frontend/gui_tests/chromedriver.exe"
    unittest.main(argv=['first-arg-is-ignored'])
