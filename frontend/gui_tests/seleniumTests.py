#Followed selenium testing structure from TexasVotes
#https://gitlab.com/forbesye/fitsbits/-/tree/master/front-end/gui_tests 

import os
from sys import platform

if __name__ == "__main__":
    if platform == "win32" :
        PATH = "./front-end/gui_tests/chromedriver.exe"
    elif platform == "linux" :
        PATH = "./frontend/gui_tests/chromedriver_linux"
    else :
        print("Unsupported OS")
        exit(-1)

    os.system("python3 ./frontend/gui_tests/splashTests.py " + PATH)
    # images on about page take too long to load on CI 
    # os.system("python3 ./frontend/gui_tests/aboutTests.py " + PATH)
    os.system("python3 ./frontend/gui_tests/breweryTests.py " + PATH)
    os.system("python3 ./frontend/gui_tests/countyTests.py " + PATH)
    os.system("python3 ./frontend/gui_tests/incidentTests.py " + PATH)
