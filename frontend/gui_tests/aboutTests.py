import unittest
from selenium import webdriver #connect to a browser instance
from selenium.webdriver.common.by import By #find elements by ...
from selenium.webdriver.chrome.options import Options #set command line switches https://peter.sh/experiments/chromium-command-line-switches/
from selenium.webdriver.chrome.service import Service
import sys

PATH = "./frontend/gui_tests/chromedriver_linux"
URL = "https://www.driveresponsibly.me/#/about"

class TestAbout(unittest.TestCase):

    #set up browser and selenium
    def setUp(self):
        s = Service(PATH)
        testoptions = Options()
        testoptions.add_argument("--headless")
        testoptions.add_argument("--no-default-browser-check")
        testoptions.add_argument("--no-sandbox")
        testoptions.add_argument("--disable-gpu")
        self.driver = webdriver.Chrome(service=s, options=testoptions)
        self.driver.get(URL)

    #close browser and selenium
    def tearDown(self):
        self.driver.quit()
    
    #clicks navbar to home page
    def testAboutToHome(self):
        self.driver.find_elements(By.CLASS_NAME, 'nav-link')[0].click() 
        header = self.driver.find_element(By.TAG_NAME, 'h1')
        self.assertEqual(header.text, "Drive Responsibly")
        assert header.text == "Drive Responsibly"

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)
        assert currentURL == URL

if __name__ == '__main__':
    try:
        PATH = sys.argv[1]
    except IndexError:
        PATH = "./frontend/gui_tests/chromedriver.exe"
    unittest.main(argv=['first-arg-is-ignored'])
